## npm
<code>npm i react-is styled-components react-base-guide --save</code>

## yarn 
<code>yarn add react-is styled-components react-base-guide</code>

[Demo](http://85.143.172.24:11000/)

### guide basics
* the guide is made using styled-components and typescript

## usage

### themes
<code>

    import React, { useState } = "react";
    import styled, { ThemeProvider } from "styled-components";
    import { themes } from "react-base-guide";
    
    const Layout = () => {
        const { theme, setTheme } = useState("default");
        const CurrentTheme = themes[theme];
        
        return (
            <ThemeProvider theme={themes[theme]}>
                {Your app}
            </ThemeProvider>
        )
    };
    
</code>

two themes are now available (default and dark), but you can create your own

### components
<code>

    import { Container, Label, Input, Datepicker //and more } from "react-base-guide";
    
    <Input 
        label="Some input"
        value={someValue}
        onChange={e => handler(e.target.value)}
    />
    
</code>

### ssr
<code>

    import { ServerStyleSheet, StyleSheetManager } from "styled-components";
    const sheet = new ServerStyleSheet();
    
    // tags need to be added to html markup
    const styleTags = sheet.getStyleElement();
    
    const HtmlContent = <StyleSheetManager sheet={sheet.instance}>
        {Your app}
    </StyleSheetManager>
    
</code>

### documentation
more complete documentation will come later
