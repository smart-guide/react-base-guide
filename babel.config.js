module.exports = function (api) {
    api.cache(true);
    const presets = [
        "@babel/preset-env",
        "@babel/preset-react",
        "@babel/preset-typescript"
    ];
    const plugins = [
        ["babel-plugin-styled-components", {
            "ssr": true,
            "pure": true,
            "minify": true,
            "transpileTemplateLiterals": true
        }]
    ];
    return {
        sourceType: "unambiguous",
        presets,
        plugins
    }
};
