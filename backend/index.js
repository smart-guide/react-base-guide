import express from "express";
import compression from "compression";
import path from "path";

const app = express();
const port = 11000;

app.disable("x-powered-by");
app.use(compression());
app.use(express.static(path.resolve(__dirname, "public")));

app.listen(port);
process.env.NODE_ENV === "development" && console.log(`Click for open project http://localhost:${port}`);

app.get("*", (req, res) => {
    res.send(`
        <!DOCTYPE html>
        <html lang="ru">
            <head>
                <base href="/">
                <meta charset="UTF-8">
                <meta name="viewport" content="width=device-width">
            </head>
            <body>
                <div id="root" class="root"></div>
                <script defer src="./bundle.js"></script>
                <script>
                    process=${JSON.stringify(process)};
                </script>
            </body>
        </html>
    `)
});
