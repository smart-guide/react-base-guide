import React from "react";
import { BrowserRouter } from "react-router-dom";
import { HelmetProvider } from "react-helmet-async";
import Router from "./router";
import Layout from "./layout";


const App = () => {
    return (
        <BrowserRouter>
            <HelmetProvider>
                <Layout>
                    <Router />
                </Layout>
            </HelmetProvider>
        </BrowserRouter>
    )
}

export default App;
