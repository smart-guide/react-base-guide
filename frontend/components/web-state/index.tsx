import React, {ReactElement} from "react";
import styled from "styled-components";
import { Input, Checkbox } from "../../../src/components";

const Grid = styled.div`
  display: grid;
  grid-gap: 24px;
  grid-template-columns: repeat(3, 1fr);
  align-items: center;
  padding: 24px 0;
`;

const Title = styled.h3`
  font-size: 18px;
  line-height: 1.5;
  font-weight: 400;
  color: ${p => p.theme.palette.text.primary};
  margin-top: 60px;
  margin-bottom: 12px;
`;

type WebStateProps = {
    children: ReactElement;
    state: Record<string, any>;
    setState: Function;
}

const ChangeControl = ({ field, value, onChange }) => {
    if (field === "value" || field === "children") { return null; }

    switch (typeof value) {
        case "number":
        case "string": {
            return <Input label={field} value={value as string} onChange={e => onChange(field, e.target.value)} />
        }
        case "boolean": {
            return <Checkbox label={field} checked={value} onChange={e => onChange(field, !value)} />
        }
        case "object": {
            return (
                <>
                    {
                        Object.keys(value).map(objField => (
                            <Input key={objField} label={objField} value={value[objField]} onChange={e => onChange(field, { ...value, [objField]: e.target.value })} />
                        ))
                    }
                </>
            )
        }
    }
}

const WebState = ({ children, state, setState }: WebStateProps) => {
    return (
        <div>
            <Title>Component</Title>
            {children}

            <Title>Fields</Title>
            <Grid>
                {Object.keys(state)?.map(field => {
                    return (
                        <ChangeControl
                            key={field}
                            field={field}
                            value={state[field]}
                            onChange={setState}
                        />
                    )
                })}
            </Grid>
        </div>
    )
}

export default WebState;
