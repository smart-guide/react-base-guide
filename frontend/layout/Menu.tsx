import React, { useState, useEffect } from "react";
import styled from "styled-components";
import { Flex, Link } from "../../src/components";


const MenuRoot = styled.header`
  position: relative;
  display: block;
  box-sizing: border-box;
  overflow: auto;
  width: 200px;
  left: 0;
  top: 0;
  padding: 15px;
  background: ${p => p.theme.palette.background.primary};
  box-shadow: ${p => p.theme.shadow.element};
  z-index: ${p => p.theme.zIndex.fixed};
`;

const Title = styled(Link)`
  color: ${p => p.theme.palette.text.primary};
  font-size: 16px;
  line-height: 60px;
`;

const linksJson = [
    { to: "/", name: "Input" },
    { to: "/textarea", name: "Textarea" },
    { to: "/checkbox", name: "Checkbox" },
    { to: "/radio", name: "Radio" },
    { to: "/range", name: "Range" },
    { to: "/button", name: "Button" },
    { to: "/icon-button", name: "Icon Button" },
    { to: "/link", name: "Link" },
    { to: "/autocomplete", name: "Autocomplete" },
    { to: "/dropdown", name: "Dropdown" },
    { to: "/dropdown-button", name: "DropdownButton" },
    { to: "/select", name: "Select" },
    { to: "/datepicker", name: "Datepicker" },
    { to: "/modal", name: "Modal" },
    { to: "/accordion", name: "Accordion" },
    { to: "/toast", name: "Toast" },
    { to: "/slider", name: "Slider" },
]

const Menu = ({ selectedTheme, setTheme }) => {
    const [ pathname, setPathname ] = useState(null);
    useEffect(() => {
        setPathname(location.pathname);
    }, []);

    const goto = (pathname) => setPathname(pathname)

    return (
        <MenuRoot>
            <Title
                variant="simple"
                to="/"
                onClick={() => goto("/")}
            >Components</Title>

            <Flex flexDirection="column" alignItems="flex-start">
                {linksJson.map(link => (
                    <Link
                        key={link.name}
                        to={link.to}
                        color={pathname === link.to ? "primary" : undefined}
                        variant={pathname === link.to ? "underline" : undefined}
                        onClick={() => goto(link.to)}
                        styled={{ marginBottom: "12px" }}
                    >{link.name}</Link>
                ))}
            </Flex>


            <Flex flexDirection="column" alignItems="flex-start">
                <Title>Themes</Title>
                <Link
                    color={selectedTheme === "light" ? "primary" : undefined}
                    variant={selectedTheme === "light" ? "underline" : undefined}
                    onClick={() => setTheme("light")}
                    styled={{ marginBottom: "12px" }}
                >Default</Link>
                <Link
                    color={selectedTheme === "dark" ? "primary" : undefined}
                    variant={selectedTheme === "dark" ? "underline" : undefined}
                    onClick={() => setTheme("dark")}
                >Dark</Link>
            </Flex>
        </MenuRoot>
    );
}

export default Menu;
