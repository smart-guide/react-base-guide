import React from "react";
import { Helmet } from "react-helmet-async";

const Meta = () => (
    <Helmet>
        <html lang="en" />
        <meta charSet="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="theme-color" content="#688CFE" />
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500&display=swap" rel="stylesheet" />

        <title>React base guide</title>

        <style>{`
            html {
                height: 100%;
            }
                
            body {
                display: block;
                position: relative;
                box-sizing: border-box;
                margin: 0;
                padding: 0;
                height: 100%;
                -webkit-font-smoothing: antialiased;
            }
                
            .root {
                min-height: 100%;
                height: 1px;
            }
            
            .root * {
                font-family: "Roboto", sans-serif;
            }
        `}</style>
    </Helmet>
);

export default Meta;
