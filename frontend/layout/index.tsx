import React, { useState } from "react";
import styled, { ThemeProvider } from "styled-components";
import Meta from "./Meta";
import theme from "../../src/themes";
import Menu from "./Menu";
import { Container } from "../../src/components";

const LayoutWrapper = styled.div`
  position: relative;
  box-sizing: border-box;
  display: flex;
  flex-direction: row;
  min-height: 100%;
  background: ${p => p.theme.palette.background.primary};
`;

const Layout = (props) => {
    const [selectedTheme, setTheme] = useState("light");

    return (
        <ThemeProvider theme={theme[selectedTheme]}>
            <Meta />
            <LayoutWrapper>
                <Menu
                    selectedTheme={selectedTheme}
                    setTheme={setTheme}
                />
                <Container>{props.children}</Container>
            </LayoutWrapper>
        </ThemeProvider>
    )
}

export default Layout;
