import React, {useState} from "react";
import Accordion from "@components/accordion";
import { AccordionProps } from "@components/accordion/interface";
import WebState from "../components/web-state";

const defaultProps: AccordionProps = {
    head: "Some head",
    children: "Some body",
    open: false,
    disabled: false,

    styled: {
        display: "inline-block",
        width: "",
        minWidth: "",
        maxWidth: "",
        height: "",
        minHeight: "",
        maxHeight: "",
        margin: "",
        marginTop: "",
        marginBottom: "",
        marginLeft: "",
        marginRight: "",
    }
}

const AccordionPage = () => {
    const [state, setAllState] = useState<AccordionProps>(defaultProps);
    const setState = (field, value) => setAllState({ ...state, [field]: value });

    return (
        <WebState
            state={state}
            setState={setState}
        >
            <Accordion {...state} />
        </WebState>
    )
}

export default AccordionPage;
