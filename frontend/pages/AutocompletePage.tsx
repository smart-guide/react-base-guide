import React, {useState} from "react";
import AutoComplete from "@components/autocomplete";
import Props from "@components/autocomplete/interface";
import WebState from "../components/web-state";


const defaultProps: Props = {
    label: "Label",
    up: false,
    right: false,
    menuWidth: "",
    menuHeight: "",
    value: "",
    placeholder: "Placeholder",
    message: "Some message",
    disabled: false,
    readOnly: false,
    clear: true,

    error: false,
    warning: false,
    success: false,

    max: 12,
    min: 0,
    maxLength: 25,

    type: "email",
    autoComplete: "on",

    styled: {
        display: "inline-block",
        width: "",
        minWidth: "",
        maxWidth: "",
        height: "",
        minHeight: "",
        maxHeight: "",
        margin: "",
        marginTop: "",
        marginBottom: "",
        marginLeft: "",
        marginRight: "",
    }
}

const AutocompletePage = () => {
    const [state, setAllState] = useState<Props>(defaultProps);
    const setState = (field, value) => setAllState({ ...state, [field]: value });

    return (
        <WebState
            state={state}
            setState={setState}
        >
            <AutoComplete
                {...state}
                onChange={e => setState("value", e.target.value)}
                rows={[
                    { value: "Zero", selected: state.value === "Zero", onClick: () => setState("value", "Zero") },
                    { value: "One", selected: state.value === "One", onClick: () => setState("value", "One") },
                    { value: "Two", selected: state.value === "Two", onClick: () => setState("value", "Two") },
                    { value: "Three", selected: state.value === "Three", onClick: () => setState("value", "Three") },
                ]}
            />
        </WebState>
    )
}

export default AutocompletePage;
