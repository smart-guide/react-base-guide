import React, {useState} from "react";
import Button from "@components/button";
import ButtonProps from "@components/button/interface";
import WebState from "../components/web-state";

const defaultProps: ButtonProps = {
    children: "Button",

    progress: false,
    secondary: false,
    disabled: false,

    error: false,
    success: false,

    styled: {
        display: "inline-block",
        width: "",
        minWidth: "",
        maxWidth: "",
        height: "",
        minHeight: "",
        maxHeight: "",
        margin: "",
        marginTop: "",
        marginBottom: "",
        marginLeft: "",
        marginRight: "",
    }
}

const ButtonPage = () => {
    const [state, setAllState] = useState<ButtonProps>(defaultProps);
    const setState = (field, value) => setAllState({ ...state, [field]: value });

    return (
        <WebState
            state={state}
            setState={setState}
        >
            <Button {...state} />
        </WebState>
    )
}

export default ButtonPage;
