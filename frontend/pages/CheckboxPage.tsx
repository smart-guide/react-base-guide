import React, {useState} from "react";
import Checkbox from "@components/checkbox";
import Props from "@components/checkbox/interface";
import WebState from "../components/web-state";

const defaultProps: Props = {
    label: "Label",
    checked: false,
    disabled: false,


    styled: {
        display: "inline-block",
        width: "",
        minWidth: "",
        maxWidth: "",
        height: "",
        minHeight: "",
        maxHeight: "",
        margin: "",
        marginTop: "",
        marginBottom: "",
        marginLeft: "",
        marginRight: "",
    }
}

const RadioPage = () => {
    const [state, setAllState] = useState<Props>(defaultProps);
    const setState = (field, value) => setAllState({ ...state, [field]: value });

    return (
        <WebState
            state={state}
            setState={setState}
        >
            <Checkbox {...state} />
        </WebState>
    )
}

export default RadioPage;
