import React, {useState} from "react";
import Datepicker from "@components/datepicker";
import { DatePickerProps } from "@components/datepicker/interface";
import WebState from "../components/web-state";

const defaultProps: DatePickerProps = {
    value: new Date(),
    label: "Label",
    message: "",
    error: false,
    warning: false,
    success: false,
    up: false,
    right: false,
    menuWidth: "",
    menuHeight: "",

    styled: {
        display: "inline-block",
        width: "",
        minWidth: "",
        maxWidth: "",
        height: "",
        minHeight: "",
        maxHeight: "",
        margin: "",
        marginTop: "",
        marginBottom: "",
        marginLeft: "",
        marginRight: "",
    }
}

const DatepickerPage = () => {
    const [state, setAllState] = useState<DatePickerProps>(defaultProps);
    const setState = (field, value) => setAllState({ ...state, [field]: value });

    return (
        <WebState
            state={state}
            setState={setState}
        >
            <Datepicker {...state} onChange={date => setState("value", date)} />
        </WebState>
    )
}

export default DatepickerPage;
