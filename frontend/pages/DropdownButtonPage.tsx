import React, {useState} from "react";
import DropdownButton from "@components/dropdown-button";
import DropdownButtonProps from "@components/dropdown-button/interface";
import WebState from "../components/web-state";
import ArrowIcon from "../../src/icons/arrow";

const defaultProps: DropdownButtonProps = {
    up: false,
    right: false,
    progress: false,

    secondary: false,
    success: false,
    error: false,

    icon: false,
    disabled: false,
    size: "s",
    menuWidth: "",
    menuHeight: "",

    styled: {
        display: "inline-block",
        width: "",
        minWidth: "",
        maxWidth: "",
        height: "",
        minHeight: "",
        maxHeight: "",
        margin: "",
        marginTop: "",
        marginBottom: "",
        marginLeft: "",
        marginRight: "",
    }
}

const DropdownPage = () => {
    const [state, setAllState] = useState<DropdownButtonProps>(defaultProps);
    const [selectedIndex, setSelectedIndex] = useState(0);
    const setState = (field, value) => setAllState({ ...state, [field]: value });

    return (
        <WebState
            state={state}
            setState={setState}
        >
            <DropdownButton
                {...state}
                rows={[
                    { value: "Zero", selected: selectedIndex === 0, onClick: () => setSelectedIndex(0) },
                    { value: "One", selected: selectedIndex === 1, onClick: () => setSelectedIndex(1) },
                    { value: "Two", selected: selectedIndex === 2, onClick: () => setSelectedIndex(2) },
                    { value: "Three", selected: selectedIndex === 3, onClick: () => setSelectedIndex(3) },
                ]}
            >
                {state.icon ? <ArrowIcon /> : "Button"}
            </DropdownButton>
        </WebState>
    )
}

export default DropdownPage;
