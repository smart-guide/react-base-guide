import React, {useState} from "react";
import Dropdown from "@components/dropdown";
import { DropdownProps } from "@components/dropdown/interface";
import WebState from "../components/web-state";


const defaultProps: DropdownProps = {
    label: "Label",
    up: false,
    right: false,
    menuWidth: "",
    menuHeight: "",

    styled: {
        display: "inline-block",
        width: "",
        minWidth: "",
        maxWidth: "",
        height: "",
        minHeight: "",
        maxHeight: "",
        margin: "",
        marginTop: "",
        marginBottom: "",
        marginLeft: "",
        marginRight: "",
    }
}

const DropdownPage = () => {
    const [state, setAllState] = useState<DropdownProps>(defaultProps);
    const [selectedIndex, setSelectedIndex] = useState(0);
    const setState = (field, value) => setAllState({ ...state, [field]: value });

    return (
        <WebState
            state={state}
            setState={setState}
        >
            <Dropdown
                {...state}
                rows={[
                    { value: "Zero", selected: selectedIndex === 0, onClick: () => setSelectedIndex(0) },
                    { value: "One", selected: selectedIndex === 1, onClick: () => setSelectedIndex(1) },
                    { value: "Two", selected: selectedIndex === 2, onClick: () => setSelectedIndex(2) },
                    { value: "Three", selected: selectedIndex === 3, onClick: () => setSelectedIndex(3) },
                ]}
            />
        </WebState>
    )
}

export default DropdownPage;
