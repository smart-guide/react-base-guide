import React, {useState} from "react";
import IconButton from "@components/icon-button";
import PlayIcon from "@components/audio/icons/play";
import IconButtonProps from "@components/icon-button/interface";
import WebState from "../components/web-state";


const defaultProps: IconButtonProps = {
    secondary: false,
    autoFocus: false,
    size: "s",
    shadow: false,

    styled: {
        display: "inline-block",
        width: "",
        minWidth: "",
        maxWidth: "",
        height: "",
        minHeight: "",
        maxHeight: "",
        margin: "",
        marginTop: "",
        marginBottom: "",
        marginLeft: "",
        marginRight: "",
    }
}

const IconButtonPage = () => {
    const [state, setAllState] = useState<IconButtonProps>(defaultProps);
    const setState = (field, value) => setAllState({ ...state, [field]: value });

    return (
        <WebState
            state={state}
            setState={setState}
        >
            <IconButton {...state}>
                <PlayIcon />
            </IconButton>
        </WebState>
    )
}

export default IconButtonPage;
