import React, {useState} from "react";
import Input from "@components/input";
import InputProps from "@components/input/interface";
import WebState from "../components/web-state";

const defaultProps: InputProps = {
    label: "Label",
    placeholder: "Placeholder",
    message: "Some message",
    disabled: false,
    readOnly: false,
    clear: true,

    error: false,
    warning: false,
    success: false,

    max: 12,
    min: 0,
    maxLength: 25,

    type: "email",
    autoComplete: "on",

    styled: {
        display: "inline-block",
        width: "",
        minWidth: "",
        maxWidth: "",
        height: "",
        minHeight: "",
        maxHeight: "",
        margin: "",
        marginTop: "",
        marginBottom: "",
        marginLeft: "",
        marginRight: "",
    }
}

const InputPage = () => {
    const [state, setAllState] = useState<InputProps>(defaultProps);
    const setState = (field, value) => setAllState({ ...state, [field]: value });

    return (
        <WebState
            state={state}
            setState={setState}
        >
            <Input {...state} />
        </WebState>
    )
}

export default InputPage;
