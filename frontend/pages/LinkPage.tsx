import React, {useState} from "react";
import Link from "@components/link";
import LinkProps from "@components/link/interface";
import WebState from "../components/web-state";


const defaultProps: LinkProps = {
    children: "Link",

    variant: "underline",
    color: "primary",
    disabled: false,
    href: "",
    to: "",

    styled: {
        display: "inline-block",
        width: "auto",
        minWidth: "",
        maxWidth: "",
        height: "auto",
        minHeight: "",
        maxHeight: "",
        margin: "0px",
        marginTop: "0px",
        marginBottom: "0px",
        marginLeft: "0px",
        marginRight: "0px",
    }
}

const LinkPage = () => {
    const [state, setAllState] = useState<LinkProps>(defaultProps);
    const setState = (field, value) => setAllState({ ...state, [field]: value });

    return (
        <WebState
            state={state}
            setState={setState}
        >
            <Link {...state} />
        </WebState>
    )
}

export default LinkPage;
