import React, {useState} from "react";
import Modal from "@components/modal";
import { ModalProps } from "@components/modal/interface";
import WebState from "../components/web-state";

const defaultProps: ModalProps = {
    open: false,
    onClose: undefined,

    maxWidth: "",
    maxHeight: "",

    // states
    layer: false,
    hideClose: false,

    // content
    title: ""
}

const ModalPage = () => {
    const [state, setAllState] = useState<ModalProps>(defaultProps);
    const setState = (field, value) => setAllState({ ...state, [field]: value });

    return (
        <WebState
            state={state}
            setState={setState}
        >
            <Modal {...state} onClose={() => setState("open", false)} />
        </WebState>
    )
}

export default ModalPage;
