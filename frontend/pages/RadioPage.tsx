import React, {useState} from "react";
import Radio from "@components/radio";
import Props from "@components/radio/interface";
import WebState from "../components/web-state";

const defaultProps: Props = {
    label: "Label",
    checked: false,
    disabled: false,


    styled: {
        display: "inline-block",
        width: "",
        minWidth: "",
        maxWidth: "",
        height: "",
        minHeight: "",
        maxHeight: "",
        margin: "",
        marginTop: "",
        marginBottom: "",
        marginLeft: "",
        marginRight: "",
    }
}

const RadioPage = () => {
    const [state, setAllState] = useState<Props>(defaultProps);
    const setState = (field, value) => setAllState({ ...state, [field]: value });

    return (
        <WebState
            state={state}
            setState={setState}
        >
            <Radio {...state} />
        </WebState>
    )
}

export default RadioPage;
