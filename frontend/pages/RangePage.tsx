import React, {useState} from "react";
import Range from "@components/range";
import { RangeProps } from "@components/range/interface";
import WebState from "../components/web-state";

const defaultProps: RangeProps = {
    value: 0,
    min: -5,
    max: 5,
    step: 1,
    count: true,
    disabled: false,

    styled: {
        display: "flex",
        width: "",
        minWidth: "",
        maxWidth: "",
        height: "",
        minHeight: "",
        maxHeight: "",
        margin: "",
        marginTop: "",
        marginBottom: "",
        marginLeft: "",
        marginRight: "",
    }
}

const RangePage = () => {
    const [state, setAllState] = useState<RangeProps>(defaultProps);
    const setState = (field, value) => setAllState({ ...state, [field]: value });

    return (
        <WebState
            state={state}
            setState={setState}
        >
            <Range {...state} onChange={e => setState("value", e.target.value)} />
        </WebState>
    )
}

export default RangePage;
