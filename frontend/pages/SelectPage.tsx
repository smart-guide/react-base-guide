import React, {useState} from "react";
import Select from "@components/select";
import {SelectProps} from "@components/select/interface";
import WebState from "../components/web-state";

const defaultProps: SelectProps = {
    value: "",
    label: "Label",
    placeholder: "Placeholder",

    disabled: false,
    up: false,
    right: false,

    menuWidth: "",
    menuHeight: "",

    styled: {
        display: "inline-block",
        width: "",
        minWidth: "",
        maxWidth: "",
        height: "",
        minHeight: "",
        maxHeight: "",
        margin: "",
        marginTop: "",
        marginBottom: "",
        marginLeft: "",
        marginRight: "",
    }
}

const SelectPage = () => {
    const [state, setAllState] = useState<SelectProps>(defaultProps);
    const [selectedIndexes, setSelectedIndexes] = useState<number[]>([])
    const setState = (field, value) => setAllState({ ...state, [field]: value });

    const clickHandler = (index) => {
        let newSelected = selectedIndexes.includes(index) ?
            selectedIndexes.filter(r => r != index) :
            [...selectedIndexes, index];
        setSelectedIndexes(newSelected);
    }

    return (
        <WebState
            state={state}
            setState={setState}
        >
            <Select
                {...state}
                rows={[
                    { value: "Zero", selected: selectedIndexes.includes(0), onClick: () => clickHandler(0) },
                    { value: "One", selected: selectedIndexes.includes(1), onClick: () => clickHandler(1) },
                    { value: "Two", selected: selectedIndexes.includes(2), onClick: () => clickHandler(2) },
                    { value: "Three", selected: selectedIndexes.includes(3), onClick: () => clickHandler(3) },
                ]}
            />
        </WebState>
    )
}

export default SelectPage;
