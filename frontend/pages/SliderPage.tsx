import React, {useState} from "react";
import styled from "styled-components";
import Slider from "@components/slider";
import { SliderProps } from "@components/slider/interface";
import WebState from "../components/web-state";

const defaultProps: SliderProps = {
    styled: {
        display: "block",
        width: "",
        minWidth: "",
        maxWidth: "",
        height: "",
        minHeight: "",
        maxHeight: "",
        margin: "",
        marginTop: "",
        marginBottom: "",
        marginLeft: "",
        marginRight: "",
    }
}

const TestSlide = styled.div<{ $color: string }>`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-around;
  width: 100%;
  height: 100%;
  background: ${p => p.theme.palette.element[p.$color] || p.theme.palette.element.primary};
  font-size: 24px;
  font-weight: bold;
  color: ${p => p.theme.palette.text.primary};
`;

const ToastPage = () => {
    const [state, setAllState] = useState<SliderProps>(defaultProps);
    const setState = (field, value) => setAllState({ ...state, [field]: value });

    return (
        <WebState
            state={state}
            setState={setState}
        >
            <Slider {...state}>
                <TestSlide $color="primary">Slide 1</TestSlide>
                <TestSlide $color="error">Slide 2</TestSlide>
                <TestSlide $color="warning">Slide 3</TestSlide>
                <TestSlide $color="success">Slide 4</TestSlide>
            </Slider>
        </WebState>
    )
}

export default ToastPage;
