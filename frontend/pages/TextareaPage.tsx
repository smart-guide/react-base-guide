import React, {useState} from "react";
import Textarea from "@components/textarea";
import TextareaProps from "@components/textarea/interface";
import WebState from "../components/web-state";

const defaultProps: TextareaProps = {
    label: "Label",
    placeholder: "Placeholder",
    message: "Some message",
    disabled: false,
    readOnly: false,
    autosize: true,

    error: false,
    warning: false,
    success: false,

    maxLength: 2000,

    autoComplete: "off",

    styled: {
        display: "inline-block",
        width: "",
        minWidth: "",
        maxWidth: "",
        height: "",
        minHeight: "",
        maxHeight: "",
        margin: "",
        marginTop: "",
        marginBottom: "",
        marginLeft: "",
        marginRight: "",
    }
}

const TextareaPage = () => {
    const [state, setAllState] = useState<TextareaProps>(defaultProps);
    const setState = (field, value) => setAllState({ ...state, [field]: value });

    return (
        <WebState
            state={state}
            setState={setState}
        >
            <Textarea {...state} />
        </WebState>
    )
}

export default TextareaPage;
