import React, {useState} from "react";
import Toast from "@components/toast";
import ToastProps from "@components/toast/interface";
import WebState from "../components/web-state";

const defaultProps: ToastProps = {
    color: "primary",
}

const ToastPage = () => {
    const [state, setAllState] = useState<ToastProps>(defaultProps);
    const setState = (field, value) => setAllState({ ...state, [field]: value });

    return (
        <WebState
            state={state}
            setState={setState}
        >
            <Toast {...state}>Some message</Toast>
        </WebState>
    )
}

export default ToastPage;
