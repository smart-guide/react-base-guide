import React from "react";
import { Route, Routes } from "react-router-dom";

import Sandbox from "./sandbox";

import InputPage from "./pages/InputPage";
import CheckboxPage from "./pages/CheckboxPage";
import RadioPage from "./pages/RadioPage";
import TextareaPage from "./pages/TextareaPage";
import RangePage from "./pages/RangePage";

import ButtonPage from "./pages/ButtonPage";
import IconButtonPage from "./pages/IconButtonPage";
import LinkPage from "./pages/LinkPage";
import AutocompletePage from "./pages/AutocompletePage";
import DropdownPage from "./pages/DropdownPage";
import DropdownButtonPage from "./pages/DropdownButtonPage";
import SelectPage from "./pages/SelectPage";
import DatepickerPage from "./pages/DatepickerPage";
import ModalPage from "./pages/ModalPage";
import AccordionPage from "./pages/AccordionPage";
import ToastPage from "./pages/ToastPage";
import SliderPage from "./pages/SliderPage";


const Router = () => (
    <Routes>
        <Route path="/sandbox" element={<Sandbox />} />

        <Route path="/" element={<InputPage />} />
        <Route path="/textarea" element={<TextareaPage />} />
        <Route path="/checkbox" element={<CheckboxPage />} />
        <Route path="/radio" element={<RadioPage />} />
        <Route path="/range" element={<RangePage />} />
        <Route path="/button" element={<ButtonPage />} />
        <Route path="/icon-button" element={<IconButtonPage />} />
        <Route path="/link" element={<LinkPage />} />
        <Route path="/autocomplete" element={<AutocompletePage />} />
        <Route path="/dropdown" element={<DropdownPage />} />
        <Route path="/dropdown-button" element={<DropdownButtonPage />} />
        <Route path="/select" element={<SelectPage />} />
        <Route path="/datepicker" element={<DatepickerPage />} />
        <Route path="/modal" element={<ModalPage />} />
        <Route path="/accordion" element={<AccordionPage />} />
        <Route path="/toast" element={<ToastPage />} />
        <Route path="/slider" element={<SliderPage />} />
    </Routes>
);

export default Router;
