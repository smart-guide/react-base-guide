import React, { useState } from "react";
import { useForm } from "react-hook-form";
import {Button, Input, Textarea} from "../../src/components";


const Sandbox = () => {
    const {register, handleSubmit, formState: {errors}} = useForm({
        defaultValues: {
            number: 60,
            text: "some text"
        },
        mode: "onBlur",
        reValidateMode: "onChange",
        shouldFocusError: true,
    });

    const [num, setNum] = useState(50);

    const submit = (data) => {
        console.log("data", data);
    }

    return (
        <div>
            <form onSubmit={handleSubmit(submit)}>
                <Input
                    {...register("number", {
                        required: "Обязательное поле",
                    })}
                    error={!!errors?.number}
                    message={errors?.number?.message || " "}
                    max={99}

                    label="From react-hook-forms"
                    clear
                    autoComplete="off"
                    placeholder="Number"
                    type="number"
                    datatype="number"

                    styled={{
                        display: "block",
                        width: "100%",
                        margin: "32px"
                    }}
                />

                <Textarea
                    {...register("text", {
                        required: "Обязательное поле",
                    })}
                    error={!!errors?.text}
                    message={errors?.text?.message || " "}

                    label="From react-hook-forms"
                    autosize
                    autoComplete="off"
                    placeholder="Text"

                    styled={{
                        display: "block",
                        width: "100%",
                        margin: "32px"
                    }}
                />

                <Button>Submit</Button>
            </form>

            {/*<Input*/}
            {/*    value={num}*/}
            {/*    onChange={e => setNum(+e.target.value)}*/}
            {/*    max={99}*/}

            {/*    label="From useState"*/}
            {/*    clear*/}
            {/*    autoComplete="off"*/}
            {/*    placeholder="Телефон"*/}
            {/*    type="number"*/}
            {/*    datatype="number"*/}

            {/*    styled={{*/}
            {/*        display: "block",*/}
            {/*        width: "100%",*/}
            {/*        margin: "32px"*/}
            {/*    }}*/}
            {/*/>*/}
        </div>
    )
}

export default Sandbox;
