import styled from "styled-components";

interface AccordionBodyProps {
    $open: boolean;
    disabled: boolean;
}

const AccordionBody = styled.div<AccordionBodyProps>`
    position: relative;
    box-sizing: border-box;
    overflow-x: ${p => p.$open ? "visible" : "hidden"};
    width: 100%;
    height: ${p => p.$open ? "100%" : "0"};
    padding: ${p => p.$open ? "16px 12px" : "0 12px"};
    border: none;
    border-radius: ${p => p.theme.radius.element};
    background: ${p => p.disabled ? p.theme.palette.element.border : p.theme.palette.background.primary};
    pointer-events: ${p => p.disabled ? "none" : "all"};
    color: ${p => p.theme.palette.text.primary};
    font-size: ${p => p.$open ? "14px" : "0"};
    margin-top: -4px;
    transition: height 0.3s, padding 0.3s;
`;

export default AccordionBody;
