import React from "react";
import styled from "styled-components";
import Arrow from "../../icons/arrow";

import { AccordionButtonProps } from "./interface";

const StyledArrow = styled(Arrow)`
  position: absolute;
  right: 12px;
  top: 0;
  bottom: 0;
  margin: auto;
`;

const StyledHead = styled.header<{ disabled: boolean }>`
  position: relative;
  display: block;
  box-sizing: border-box;
  cursor: pointer;
  user-select: none;
  outline: none;
  width: 100%;
  min-height: 36px;
  line-height: 36px;
  padding: 0 32px 0 12px;
  border: none;
  border-radius: ${p => p.theme.radius.element};
  background: ${p => p.disabled ? p.theme.palette.element.border : p.theme.palette.background.primary};
  color: ${p => p.theme.palette.text.primary};
  font-size: 14px;
  text-align: left;
  pointer-events: ${p => p.disabled ? "none" : "all"};
`;

const AccordionHead = ({ children, open, disabled, onClick }: AccordionButtonProps) => {
    return (
        <StyledHead disabled={disabled} onClick={onClick}>
            <StyledArrow rotate={open ? "-180deg" : "0"} />
            {children}
        </StyledHead>
    )
}

export default AccordionHead;
