import React, {useEffect, useState} from "react";
import styled from "styled-components";
import { AccordionProps } from "./interface";

import AccordionHead from "./head";
import Body from "./body";
import {addCustomStyle, StyleProps} from "@utils/add-custom-style";


const AccordionContainer = styled.div<{styled$: StyleProps}>`
  position: relative;
  box-sizing: border-box;
  box-shadow: ${p => p.theme.shadow.element};
  ${p => addCustomStyle(p.styled$)}
`;

const Accordion = (props: AccordionProps) => {
    const { head, children } = props;
    const [ open, setOpen ] = useState(props.open || false);

    useEffect(() => setOpen(props.open), [props.open]);

    return (
        <AccordionContainer
            className={props.className}
            styled$={props.styled}
        >
            <AccordionHead
                open={open}
                disabled={props.disabled}
                onClick={() => setOpen(!open)}
            >
                {head}
            </AccordionHead>

            <Body $open={open} disabled={props.disabled}>{open ? children : null}</Body>
        </AccordionContainer>
    )
}

export default Accordion;
