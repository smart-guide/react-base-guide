import {MouseEventHandler, ButtonHTMLAttributes, ReactElement} from "react";
import { StyleProps } from "@utils/add-custom-style";

interface AccordionProps extends ButtonHTMLAttributes<HTMLButtonElement> {
    children?: ReactElement | string;
    head?: any;
    open?: boolean;
    styled?: StyleProps;
}

interface AccordionButtonProps {
    children?: ReactElement | string;
    disabled?: boolean;
    open?: boolean;
    onClick?: MouseEventHandler;
}

export { AccordionProps, AccordionButtonProps };
