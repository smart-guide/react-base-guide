import React from "react";
import { Svg, Path, IconProps } from "@components/svg";

const PauseIcon = (props: IconProps) => (
    <Svg
        {...props}
        width={props.styled?.width || "24px"}
        height={props.styled?.height || "24px"}
        viewBox="0 0 24 24"
    >
        <Path
            className={props.className ? props.className + " rgb-fill" : "rgb-fill"}
            d="M6 19h4V5H6v14zm8-14v14h4V5h-4z"
            fill={props.fill || "primary"}
        />
        <path d="M0 0h24v24H0z" fill="none"/>
    </Svg>
);

export default PauseIcon;
