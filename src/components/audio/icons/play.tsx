import React from "react";
import { Svg, Path, IconProps } from "@components/svg";

const PlayIcon = (props: IconProps) => (
    <Svg
        {...props}
        width={props.styled?.width || "24px"}
        height={props.styled?.height || "24px"}
        viewBox="0 0 24 24"
    >
        <Path
            d="M8 5v14l11-7z"
            fill={props.fill || "primary"}
            className={props.className ? props.className + " rgb-fill" : "rgb-fill"}
        />
        <path d="M0 0h24v24H0z" fill="none"/>
    </Svg>
);

export default PlayIcon;
