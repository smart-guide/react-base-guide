import React, {useEffect, useRef, useState} from "react";
import styled from "styled-components";
import IconButton from "@components/icon-button";
import Range from "@components/range";
import Label from "@components/label";
import addCustomStyle from "@utils/add-custom-style";

import PlayIcon from "./icons/play";
import PauseIcon from "./icons/pause";
import AudioProps from "./interface";


const Wrapper = styled.div<AudioProps>`
  justify-content: space-between;
  flex-wrap: nowrap;
  box-sizing: border-box;
  align-items: center;
  ${p => addCustomStyle(p.styled, "flex")}
`;

const convertSecToMin = (sec) => {
    if (!sec) { return "00:00" }
    if (sec === Infinity) { return "Infinity" }
    let minutes = Math.trunc(sec / 60);
    let remainSec = Math.round(sec - (minutes * 60));
    let stringMinutes = minutes < 10 ? "0" + minutes : minutes.toString();
    let stringSec = remainSec < 10 ? "0" + remainSec : remainSec.toString();
    return stringMinutes + ":" + stringSec;
};


const Audio = (props: AudioProps) => {
    const { src, autoPlay, onEnded, disabled } = props;
    const audioRef = useRef(null);
    const [duration, setDuration] = useState(0);
    const [currentTime, setCurrentTime] = useState(0);
    const [playing, setPlaying] = useState(false);
    const [ready, setReady] = useState(false);

    useEffect(() => {
        if (!ready) {
            audioRef.current.onloadedmetadata = async () => {
                if (audioRef.current) {
                    while(audioRef.current && (audioRef.current.duration === Infinity)) {
                        await new Promise(r => setTimeout(r, 1000));
                        if (audioRef.current) {
                            audioRef.current.currentTime = 10000000*Math.random();
                        }
                    }
                    setDuration(audioRef.current.duration);
                    audioRef.current.currentTime = 0;
                    setReady(true);
                }


                if (!playing && autoPlay) {
                    audioRef.current && audioRef.current.play();
                    setPlaying(true);
                }
            }
        }

        audioRef.current.ontimeupdate = () => {
            if (audioRef.current) {
                setCurrentTime(audioRef.current.currentTime);
            }
        };

        audioRef.current.onended = (e) => {
            if (audioRef.current) {
                audioRef.current.currentTime = 0;
                setPlaying(false);
            }
            onEnded && onEnded(e);
        }

        audioRef.current.onerror = (err) => {
            console.log("Error loading audio", err);
        }

        audioRef.current.ondurationchange = () => {
            if (audioRef.current) {
                const duration = audioRef.current.duration;
                if (duration !== Infinity) {
                    setDuration(duration);
                }
            }
        }

        playing ? audioRef.current.play() : audioRef.current.pause();

        return () => {
            if (audioRef.current) {
                audioRef.current.oncanplay = null;
                audioRef.current.ontimeupdate = null;
                audioRef.current.onended = null;
            }
        }
    }, [playing]);

    return (
        <Wrapper
            id={props.id}
            className={props.className}
            tabIndex={props.tabIndex}
            styled={props.styled}
        >
            <audio
                ref={audioRef}
                src={src}
                autoPlay={autoPlay}
                muted={false}
            />

            {playing ?
                <IconButton
                    secondary
                    onTouchStart={(e) => {
                        e.preventDefault();
                        audioRef.current.pause();
                        setPlaying(false);
                    }}
                    onMouseUp={() => setPlaying(false)}
                    onTouchEnd={e => e.preventDefault()}
                    disabled={disabled}
                ><PauseIcon /></IconButton> :
                <IconButton
                    secondary
                    onTouchStart={(e) => {
                        e.preventDefault();
                        audioRef.current.play();
                        setPlaying(true);
                    }}
                    onMouseUp={() => setPlaying(true)}
                    onTouchEnd={e => e.preventDefault()}
                    disabled={disabled}
                ><PlayIcon /></IconButton>
            }

            <Range
                onChange={e => {
                    audioRef.current.currentTime = e.target.value;
                }}
                value={currentTime}
                max={duration}
                min={0}
                styled={{ marginLeft: "12px", marginRight: "12px" }}
            />

            <Label variant="standard" label={convertSecToMin(currentTime) + "/" + convertSecToMin(duration)} />
        </Wrapper>
    )
};

export default Audio;
