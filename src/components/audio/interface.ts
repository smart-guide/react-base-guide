import {AudioHTMLAttributes} from "react";
import { StyleProps } from "@utils/add-custom-style";

interface AudioProps extends AudioHTMLAttributes<HTMLAudioElement> {
    autoPlay?: boolean;
    disabled?: boolean;
    styled?: StyleProps;
}

export default AudioProps;
