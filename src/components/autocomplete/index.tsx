import React, {ForwardedRef, forwardRef, useState} from "react";
import styled from "styled-components";
import Input from "@components/input";
import DropdownMenu from "@components/dropdown-menu";
import { addCustomStyle, StyleProps } from "@utils/add-custom-style";
import dropdownMenuKeysHandler from "@utils/dropdown-menu-keys-handler";
import AutoCompleteProps from "./interface";


const AutocompleteRoot = styled.div<{ $styled: StyleProps }>`
  position: relative;
  box-sizing: border-box;
  ${p => addCustomStyle(p.$styled, "inline-block")}
`;

const AutoComplete = (props: AutoCompleteProps, ref: ForwardedRef<any>) => {
    const [ open, setOpen ] = useState<boolean>(false);
    const rows = props.rows?.filter(row => row?.value?.toLowerCase().indexOf(props.value?.toString()?.toLowerCase()) > -1);

    return <AutocompleteRoot
        $styled={props.styled}
    >
        <Input
            styled={{ display: "block", width: "100%" }}
            label={props.label}
            message={props.message}
            type={props.type}
            placeholder={props.placeholder}
            maxLength={props.maxLength}
            value={props.value}
            onChange={props.onChange}
            ref={ref}
            id={props.id}
            name={props.name}
            tabIndex={props.tabIndex}
            max={props.max}
            min={props.min}
            disabled={props.disabled}
            readOnly={props.readOnly}
            autoComplete={props.autoComplete}
            autoFocus={props.autoFocus}
            onClick={props.onClick}
            onMouseUp={props.onMouseUp}
            onMouseDown={props.onMouseDown}
            onKeyUp={(e) => dropdownMenuKeysHandler({ e, setOpen, rows })}
            onKeyDown={props.onKeyDown}
            onKeyPress={props.onKeyPress}
            onFocus={() => setOpen(true)}
            onBlur={e => { setOpen(false); props.onBlur && props.onBlur(e); }}
            error={props.error}
            warning={props.warning}
            success={props.success}
        />

        <DropdownMenu
            rows={rows}
            open={open}
            menuWidth={props.menuWidth || "100%"}
            menuHeight={props.menuHeight}
            right={props.right}
            up={props.up}
        />
    </AutocompleteRoot>
}

export default forwardRef(AutoComplete);
