import { DropdownRowProps } from "@components/dropdown-menu/interface"
import InputProps from "@components/input/interface";

interface AutoCompleteProps extends InputProps {
    rows?: DropdownRowProps[];
    right?: boolean;
    up?: boolean;
    menuWidth?: string;
    menuHeight?: string;
}

export default AutoCompleteProps;
