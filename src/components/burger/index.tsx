import React from "react";
import styled from "styled-components";
import { addCustomStyle, StyleProps } from "@utils/add-custom-style";
import BurgerProps from "./interface";

const Wrapper = styled.button<{ $styled: StyleProps }>`
  position: relative;
  height: 30px;
  border: none;
  outline: none;
  background: transparent;
  transition: background 0.3s;
  cursor: pointer;
  user-select: none;
  z-index: ${p => p.theme.zIndex.absolute};

  ${p => addCustomStyle(p.$styled)}
`;

const Span = styled.span<{ close: boolean }>`
  display: block;
  position: absolute;
  left: 0;
  right: 0;
  top: 50%;
  height: 2px;
  background: ${p => p.theme.palette.text.primary};
  transition: background 0.3s;
  &::before, &::after {
    position: absolute;
    display: block;
    left: 0;
    width: 100%;
    height: 2px;
    background-color: ${p => p.theme.palette.text.primary};
    content: '';
    transition: top 0.3s, bottom 0.3s, transform 0.3s;
  }
  &::before {
    top: -10px;
  }
  &::after {
    bottom: -10px;
  }
  
  ${p => p.close && `
    background: none;
    &::before {
      top: 0;
      transform: rotate(45deg);
    }
    &::after {
      bottom: 0;
      transform: rotate(-45deg);
    }
  `}
`;

const getProps = (p: BurgerProps): BurgerProps => {
    let props = {...p};
    delete props.styled;
    delete props.close;
    return props;
}

const Burger = (props: BurgerProps) => (
    <Wrapper
        {...getProps(props)}
        $styled={props.styled}
    >
        <Span close={props.close} />
    </Wrapper>
);

export default Burger;
