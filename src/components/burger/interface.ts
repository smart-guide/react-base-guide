import {ButtonHTMLAttributes} from "react";
import { StyleProps } from "@utils/add-custom-style";

interface BurgerProps extends ButtonHTMLAttributes<HTMLButtonElement> {
    close?: boolean;
    styled?: StyleProps;
}

export default BurgerProps;
