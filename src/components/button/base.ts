import styled from "styled-components";
import ButtonProps from "./interface";
import addCustomStyle from "@utils/add-custom-style";

interface IGetColorRes {
    background: string;
    backgroundHover: string;
    border: string;
    borderHover: string;
    color: string;
}

const getColors = (p): IGetColorRes => {
    if (p.success) {
        return {
            background: p.theme.palette.element.success,
            backgroundHover: p.theme.palette.element.success,
            border: p.theme.palette.element.success,
            borderHover: p.theme.palette.element.success,
            color: p.theme.palette.text.contrast,
        }
    }
    if (p.error) {
        return {
            background: p.theme.palette.element.error,
            backgroundHover: p.theme.palette.element.error,
            border: p.theme.palette.element.error,
            borderHover: p.theme.palette.element.error,
            color: p.theme.palette.text.contrast,
        }
    }
    if (p.secondary) {
        return {
            background: p.theme.palette.background.primary,
            backgroundHover: p.theme.palette.background.active,
            border: p.theme.palette.element.primary,
            borderHover: p.theme.palette.element.primary,
            color: p.theme.palette.text.active
        }
    }

    return {
        background: p.theme.palette.element.primary,
        backgroundHover: p.theme.palette.element.primaryHover,
        border: p.theme.palette.element.primary,
        borderHover: p.theme.palette.element.primaryHover,
        color: p.theme.palette.text.contrast,
    }
}

const BaseButton = styled.button<ButtonProps>`
  display: ${p => p.styled?.display || "inline-block"};
  position: relative;
  box-sizing: border-box;
  cursor: pointer;
  user-select: none;
  outline: none;
  white-space: nowrap;
  overflow: hidden;
  
  height: 36px;
  width: 100%;
  padding: 0 16px;
  border: 1px solid ${p => getColors(p).border};
  border-radius: ${p => p.theme.radius.element};
  transition: background 0.3s, border 0.3s, box-shadow 0.3s;
  
  font-size: 14px;
  color: ${p => getColors(p).color};
  background: ${p => getColors(p).background};
  
  &:hover {
    background: ${p => getColors(p).backgroundHover};
    border: 1px solid ${p => getColors(p).borderHover};
    box-shadow: ${p => p.theme.shadow.element};
  }
  
    
  &:active {
    transform: translateY(1px);
  }
  
  &:disabled {
    pointer-events: none;
    background: ${p => p.theme.palette.element.disabled};
    color: ${p => p.progress ? "transparent" : p.theme.palette.text.disabled};
    border: 1px solid ${p => p.theme.palette.element.disabled};
  }
  
  ${p => addCustomStyle(p.styled)}
`;

export default BaseButton;
