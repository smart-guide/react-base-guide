import React, {forwardRef, useRef} from "react";
import Loader from "@components/loader";
import Ripple from "@components/ripple";
import BaseButton from "./base";
import ButtonProps from "./interface";

const onRef = (ref, buttonRef) => {
    return typeof ref === "function" ?
        (e) => { ref(e); buttonRef.current = e; }
        : buttonRef
}

const Button = (props: ButtonProps, ref: any) => {
    const buttonRef = (typeof ref !== "function" && !!ref) ? ref : useRef(undefined);

    return (
        <Ripple
            disabled={props.disabled || props.progress}
            className={props.className}
            styled={props.styled}
        >
            <BaseButton
                {...props}
                ref={onRef(ref, buttonRef)}
                disabled={props.disabled || props.progress}
                className={undefined}
                styled={{ display: "block" }}
            >
                {props.children}
                {props.progress && <Loader />}
            </BaseButton>
        </Ripple>
    )
}

export default forwardRef(Button);
