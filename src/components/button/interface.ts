import { ButtonHTMLAttributes } from "react";
import { StyleProps } from "@utils/add-custom-style";

interface ButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
    progress?: boolean;
    secondary?: boolean;
    success?: boolean;
    error?: boolean;

    styled?: StyleProps;
}

export default ButtonProps;
