import React from "react";
import { Svg, Rect, Polygon, IconProps } from "../../svg";

const Checked = (props: IconProps) =>
    <Svg
        {...props}
        width="18px"
        height="18px"
        verticalAlign="-5px"
        cursor="pointer"
        viewBox="0 0 16 16"
        styled={{ marginRight: "8px" }}
    >
        <g stroke="none" fill="none">
            <g>
                <Rect
                    x="0.5"
                    y="0.5"
                    rx="3"
                    width="15px"
                    height="15px"
                    strokeWidth="1px"
                    stroke="secondary"
                />
                <Polygon
                    points="6.80297024 11.9494508 3 8.20315165 4.29388389 6.80369946 6.80297024 9.34124384 11.7271376 4 13 5.19017765"
                    fill="active"
                />
            </g>
        </g>
    </Svg>;

export default Checked;
