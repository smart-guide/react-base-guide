import React from "react";
import { Svg, Rect, IconProps } from "@components/svg";

const Unchecked = (props: IconProps) =>
    <Svg
        {...props}
        width="18px"
        height="18px"
        verticalAlign="-5px"
        cursor="pointer"
        viewBox="0 0 16 16"
        styled={{ marginRight: "8px" }}
    >
        <g stroke="none" fill="none">
            <g stroke="none" fill="none">
                <Rect
                    width="15px"
                    height="15px"
                    x="0.5"
                    y="0.5"
                    rx="3"
                    stroke="secondary"
                    strokeWidth="1px"
                />
            </g>
        </g>
    </Svg>;

export default Unchecked;
