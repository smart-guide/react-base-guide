import React from "react";
import styled from "styled-components";
import Label from "@components/label";
import CheckedIcon from "./icons/checked";
import UncheckedIcon from "./icons/unchecked";
import CheckboxProps from "./interface";


const CheckboxInput = styled.input`
  display: none;
`;

const CheckboxLabel = styled(Label)`
  opacity: ${p => p.disabled ? "0.7" : "1"};
  pointer-events: ${p => p.disabled ? "none": "all"};
  
  &:hover {
    color: ${p => p.theme.palette.element.primary};
    
    rect {
      stroke: ${p => p.theme.palette.element.primary};
    }
  }
`;

const Checkbox = (props: CheckboxProps) => (
    <CheckboxLabel
        variant="standard"
        disabled={props.disabled}
        className={props.className}
        styled={props.styled}
        label={
            <>
                {props.checked ? <CheckedIcon disabled={props.disabled} /> : <UncheckedIcon disabled={props.disabled} />}
                {props.label}
            </>
        }
    >
        <CheckboxInput
            type="checkbox"
            disabled={props.disabled}
            checked={props.checked || false}
            onChange={props.onChange}
        />
    </CheckboxLabel>
);
export default Checkbox;
