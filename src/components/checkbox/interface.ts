import {InputHTMLAttributes, ReactElement} from "react";
import { StyleProps } from "@utils/add-custom-style";

interface CheckboxProps extends InputHTMLAttributes<HTMLInputElement> {
    label?: ReactElement | string;
    checked?: boolean;
    styled?: StyleProps;
}

export default CheckboxProps;
