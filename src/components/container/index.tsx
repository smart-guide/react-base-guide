import React from "react";
import styled from "styled-components";
import addCustomStyle from "@utils/add-custom-style";
import ContainerProps from "./interface";

const Container = styled.section<ContainerProps>`  
  position: relative;
  box-sizing: border-box;
  padding: 0 36px;

  @media(max-width: ${p => p.theme.screen.mobile}) {
    padding: 0 10px;
  }

  ${p => addCustomStyle(p.styled)}
`;

export default Container;
