import {HTMLProps} from "react";
import { StyleProps } from "@utils/add-custom-style";

interface IContainer extends HTMLProps<HTMLDivElement>{
    children?: any;
    styled?: StyleProps;
}

export default IContainer;
