import React from "react";
import styled from "styled-components";
import { addCustomStyle, StyleProps } from "@utils/add-custom-style";

const DatepickerRoot = styled.div<{ $styled: StyleProps }>`
  position: relative;
  box-sizing: border-box;
  display: inline-block;
  ${p => addCustomStyle(p.$styled, "inline-block")}
`;

export default DatepickerRoot;
