import { IDatePickerConfig } from "./interface";

const config: IDatePickerConfig = {
    input: "dd.mm.yyyy",
    daysOfWeek: ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"],
    months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
}

export default config
