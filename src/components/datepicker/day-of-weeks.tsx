import React from "react";
import styled from "styled-components";

const Week = styled.div`
  box-sizing: border-box;
  display: flex;
  justify-content: space-between;
  width: 100%;
  user-select: none;
  font-size: 12px;
  opacity: 0.7;
  pointer-events: none;
`;

const Day = styled.div`
  box-sizing: border-box;
  width: 14%;
  height: 32px;
  line-height: 32px;
  text-align: center;
  border-radius: ${p => p.theme.radius.element};
  color: ${p => p.theme.palette.text.primary};
  transition: background 0.3s, box-shadow 0.3s;
  &:nth-child(7n + 7), &:nth-child(7n + 6) {
    color: ${p => p.theme.palette.text.error};
  }
`;

const DaysOfWeek = ({ days }) => (
    <Week>
        {days && days.map((day, key) => (<Day key={key}>{day}</Day>))}
    </Week>
);

export default DaysOfWeek;
