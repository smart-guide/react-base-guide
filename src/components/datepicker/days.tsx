import React from "react";
import styled from "styled-components";
import dateFormat from "dateformat";

const DaysWrapper = styled.div`
  box-sizing: border-box;
  display: flex;
  flex-wrap: wrap;
  width: 100%;
  user-select: none;
`;

const Day = styled.div<{ $active: boolean, disabled: boolean }>`
  box-sizing: border-box;
  width: 14%;
  height: 32px;
  line-height: 32px;
  text-align: center;
  border-radius: ${p => p.theme.radius.element};
  color: ${p => p.$active ? p.theme.palette.text.contrast : p.theme.palette.text.primary};
  background: ${p => p.$active ? p.theme.palette.element.primary : "none"};
  transition: background 0.3s, box-shadow 0.3s;
  pointer-events: ${p => (p.$active || p.disabled) ? "none" : "all"};

  &:hover {
    background: ${p => p.theme.palette.background.active};
    box-shadow: 0 0 0 1px ${p => p.theme.palette.element.primary};
  }

  &:nth-child(7n + 7), &:nth-child(7n + 6) {
    color: ${p => p.$active ? p.theme.palette.text.contrast : p.theme.palette.element.error};
    &:hover {
      background: ${p => p.theme.palette.background.error};
      box-shadow: 0 0 0 1px ${p => p.theme.palette.element.error};
    }
  }
`;

const getOffset = (startOfMonth) => {
    let weekNumber = startOfMonth.getDay();
    weekNumber = weekNumber - 1;
    weekNumber = weekNumber < 0 ? 6 : weekNumber;
    return weekNumber;
};

const getDays = ({ date }) => {
    const month = +dateFormat(date, "m");
    const year = +dateFormat(date, "yyyy");

    let result = [];

    let endOfMonth = +dateFormat(new Date(year, month, 0), "dd");
    let startOfMonth = new Date(year, month - 1);
    let weekOffset = getOffset(startOfMonth);

    for (let offset = 0; offset < weekOffset; offset++) {
        result.push(null);
    }
    for (let day = 1; day <= endOfMonth; day++) {
        result.push(day);
    }
    return result;
};

const Days = (props) => {
    const days = getDays(props);
    const currentDay = props.value ? +dateFormat(props.value, "dd") : null;
    const activeMonth = (+dateFormat(props.value, "m") === +dateFormat(props.date, "m")) && (+dateFormat(props.value, "yyyy") === +dateFormat(props.date, "yyyy"));

    return (
        <DaysWrapper>
            {days.map((day, key) => (
                <Day
                    key={key}
                    disabled={!day}
                    $active={currentDay ? (activeMonth && (currentDay === day)) : false}
                    onClick={() => props.setDay(day)}
                >{day}</Day>
            ))}
        </DaysWrapper>
    );
}

export default Days;
