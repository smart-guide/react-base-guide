import React, {useState, useRef, forwardRef} from "react";
import dateFormat from "dateformat";
import Input from "@components/input";
import Menu from "@components/dropdown-menu";
import DatepickerRoot from "./DatepickerRoot";
import DaysOfWeek from "./day-of-weeks";
import Selectors from "./selectors";
import Days from "./days";
import config from "./config";
import { DatePickerProps } from "./interface";

const setDay = ({ day, month, year, setOpen, onChange }) => {
    let value = new Date(year, month - 1, day);
    onChange(value);
    setOpen(false);
}

const displayDate = (value, month, year) => {
    let result = new Date(value);
    result.setFullYear(year, month);
    return result;
}

const onBlur = (e, setOpen, containerRef) => {
    if (!e.relatedTarget || !containerRef.current.contains(e.relatedTarget)) {
        setOpen(false);
    }
}


const Datepicker = (props: DatePickerProps, ref: any) => {
    const value = props.value || new Date();
    const [open, setOpen] = useState(false);
    const [year, setYear] = useState(+dateFormat(value, "yyyy"));
    const [month, setMonth] = useState(+dateFormat(value, "m"));
    const date = displayDate(value, month - 1, year);
    const format = props.config || config;
    const containerRef = useRef(null);

    return (
        <DatepickerRoot
            ref={containerRef}
            tabIndex={props.tabIndex || 0}
            $styled={props.styled}
            onBlur={e => onBlur(e, setOpen, containerRef)}
        >
            <Input
                ref={ref}
                className={props.className}
                label={props.label}
                placeholder={props.placeholder}
                value={props.value ? dateFormat(props.value, format.input) : ""}
                id={props.id}
                name={props.name}
                disabled={props.disabled}
                readOnly={true}
                autoComplete={props.autoComplete}
                autoFocus={props.autoFocus}
                onClick={() => setOpen(true)}
                onMouseUp={props.onMouseUp}
                onMouseDown={props.onMouseDown}
                onKeyUp={props.onKeyUp}
                onKeyDown={props.onKeyDown}
                onKeyPress={props.onKeyPress}
                clear={props.clear}
                onClear={(e) => {
                    props.onClear && props.onClear(e);
                    props.onChange(null)
                }}
                error={props.error}
                warning={props.warning}
                success={props.success}
                message={props.message}
                styled={{
                    display: "block",
                    width: "100%"
                }}
            />

            {open && <Menu
                open={open}
                menuWidth={props.menuWidth || "270px"}
                menuHeight={props.menuHeight}
                right={props.right}
                up={props.up}
            >
                <Selectors
                    month={month}
                    months={format.months}
                    setMonth={m => setMonth(m)}
                    year={year}
                    setYear={y => setYear(y)}
                    min={props.min || 1970}
                    max={props.max || new Date().getFullYear()}
                />
                <DaysOfWeek days={format.daysOfWeek} />
                <Days
                    value={props.value}
                    date={date}
                    setDay={(day) => setDay({
                        day,
                        month,
                        year,
                        setOpen,
                        onChange: props.onChange,
                    })}
                />
            </Menu>}
        </DatepickerRoot>
    )
};

export default forwardRef(Datepicker);
