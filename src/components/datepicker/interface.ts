import {ChangeEventHandler, InputHTMLAttributes} from "react";
import { StyleProps } from "@utils/add-custom-style";

interface IDatePickerConfig {
    input: string;
    daysOfWeek: string[];
    months: string[];
}

interface DatePickerProps extends Omit<InputHTMLAttributes<HTMLInputElement>, "value">{
    value: Date;
    label?: string;
    message?: string;
    error?: boolean;
    warning?: boolean;
    success?: boolean;
    autoComplete?: "on" | "off";
    clear?: boolean;
    onClear?: ChangeEventHandler;
    up?: boolean;
    right?: boolean;
    menuWidth?: string;
    menuHeight?: string;
    config?: IDatePickerConfig;
    styled?: StyleProps;
}

export { DatePickerProps, IDatePickerConfig };
