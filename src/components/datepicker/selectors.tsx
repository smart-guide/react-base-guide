import React from "react";
import styled from "styled-components";
import Dropdown from "@components/dropdown";

const SelectorsWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 8px;
`;

const months = ({ months, month, setMonth }) => {
    let result = [];
    for (let i = 1; i <= 12; i++) {
        result.push({
            value: months[i - 1],
            selected: i === month,
            onClick: () => setMonth(i)
        })
    }
    return result;
}

const years = ({ year, min, max, setYear }) => {
    let result = [];
    for (let i = max; i >= min; i--) {
        result.push({
            value: i,
            selected: i === year,
            onClick: () => setYear(i)
        })
    }
    return result;
}

const Selectors = (props) => (
    <SelectorsWrapper>
        <Dropdown rows={months(props)} menuHeight="160px" styled={{ width: "140px" }} />
        <Dropdown rows={years(props)} menuHeight="160px" styled={{ width: "100px" }} />
    </SelectorsWrapper>
);

export default Selectors;
