import React, { useRef, useState } from "react";
import styled from "styled-components";
import Button from "@components/button";
import IconButton from "@components/icon-button";
import DropdownMenu from "@components/dropdown-menu";
import { addCustomStyle, StyleProps } from "@utils/add-custom-style";
import DropdownButtonProps from "./interface";

const Wrapper = styled.div<{ $styled: StyleProps }>`
  position: relative;
  vertical-align: middle;
  ${p => addCustomStyle(p.$styled, "inline-block")}
`;

const onBlur = (e, setOpen, containerRef) => {
    if (!e.relatedTarget || !containerRef.current.contains(e.relatedTarget)) {
        setOpen(false);
    }
}

const DropdownButton = (props: DropdownButtonProps) => {
    const [open, setOpen] = useState(false);
    const containerRef = useRef(null);

    return (
        <Wrapper
            ref={containerRef}
            className={props.className}
            $styled={props.styled}
            onBlur={e => onBlur(e, setOpen, containerRef)}
        >
            {props.icon ?
                <IconButton
                    disabled={props.disabled}
                    secondary={props.secondary}
                    size={props.size}
                    onClick={() => setOpen(!open)}
                    styled={{ width: props.styled?.width }}
                >
                    {props.children}
                </IconButton> :
                <Button
                    disabled={props.disabled}
                    progress={props.progress}
                    secondary={props.secondary}
                    onClick={() => setOpen(!open)}
                    styled={{ width: props.styled?.width }}
                >
                    {props.children}
                </Button>
            }

            {(!props.progress && !props.disabled) && <DropdownMenu
                open={open}
                menuWidth={props.menuWidth || (props.icon ? null : "100%")}
                menuHeight={props.menuHeight}
                right={props.right}
                up={props.up}
                rows={props.rows}
            />}
        </Wrapper>
    );
}

export default DropdownButton;
