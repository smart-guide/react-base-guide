import {ButtonHTMLAttributes} from "react";
import { DropdownRowProps } from "@components/dropdown-menu/interface";
import {StyleProps} from "@utils/add-custom-style";


interface DropdownButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
    progress?: boolean
    right?: boolean
    up?: boolean
    icon?: boolean

    secondary?: boolean
    success?: boolean
    error?: boolean

    menuWidth?: string
    menuHeight?: string

    size?: "s" | "m" | "l"
    rows?: DropdownRowProps[]

    styled?: StyleProps
}

export default DropdownButtonProps;
