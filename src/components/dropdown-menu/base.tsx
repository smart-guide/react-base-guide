import styled, { keyframes } from "styled-components";
import { DropdownMenuProps } from "./interface";

const Down = keyframes`
  0% { transform: translateY(0) }
  100% { transform: translateY(4px) }
`;

const Up = keyframes`
  0% { transform: translateY(0) }
  100% { transform: translateY(-4px) }
`;

const Menu = styled.div<DropdownMenuProps>`
  position: absolute;
  box-sizing: border-box;
  background-color: ${p => p.theme.palette.element.background};
  width: ${p => p.menuWidth};
  max-width: ${p => p.menuWidth};
  max-height: ${p => p.menuHeight || "270px"};
  right: ${p => p.right ? "0" : "auto"};
  bottom: ${p => p.up ? "100%" : "auto"};
  
  overflow: auto;
  outline: none;
  border-radius: ${p => p.theme.radius.element};
  box-shadow: ${p => p.theme.shadow.absolute};
  text-align: left;
  padding: 8px;
  z-index: ${p => p.theme.zIndex.dropdown};
  animation: ${p => p.up ? Up : Down} 0.3s forwards;
  
  scrollbar-color: ${p => p.theme.palette.text.secondary} ${p => p.theme.palette.element.border};
  scrollbar-width: thin;
  
  ::-webkit-scrollbar {
    width: 3px;
  }
  ::-webkit-scrollbar-track {
    box-shadow: inset 0 0 6px ${p => p.theme.palette.element.border};
  }
  ::-webkit-scrollbar-thumb {
    background-color: ${p => p.theme.palette.text.secondary};
    outline: 1px solid ${p => p.theme.palette.text.secondary};
  }
`;

export default Menu;
