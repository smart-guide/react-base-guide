import React, { useRef, useEffect } from "react";
import { DropdownMenuProps } from "./interface";
import DropdownMenuBase from "./base";
import DropdownRow from "./row";

const scrollToSelected = (menuRef, selectedRef) => {
    if (menuRef?.current && selectedRef?.current) {
        (menuRef.current.scrollTop = (selectedRef.current.offsetTop - selectedRef.current.offsetHeight));
    }
}

const DropdownMenu = (props: DropdownMenuProps) => {
    const { open, up, right, menuHeight, menuWidth, rows, children, forwardRef } = props;
    const menuRef = useRef(null);
    const selectedRef = useRef(null);

    useEffect(() => {
        if (open) { scrollToSelected(menuRef, selectedRef); }
    }, [open, rows]);

    if (!open || (!rows && !children || rows?.length === 0)) {
        return null;
    }

    return (
        <DropdownMenuBase
            ref={forwardRef || menuRef}
            className={props.className}
            open={open}
            up={up}
            right={right}
            menuHeight={menuHeight}
            menuWidth={menuWidth}
        >
            {children}
            {rows?.map((row, key) => (
                <DropdownRow
                    forwardRef={row.selected ? selectedRef : undefined}
                    key={`${row.value}-${key}`}
                    value={row.value}
                    selected={row.selected}
                    onMouseDown={row.onClick}
                />
            ))}
        </DropdownMenuBase>
    )
}

export default DropdownMenu;
