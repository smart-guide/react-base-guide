import {MouseEventHandler} from "react";

interface DropdownRowProps {
    key?: any
    forwardRef?: any
    value: string
    selected?: boolean
    onClick?: MouseEventHandler
    onMouseDown?: MouseEventHandler
}

interface DropdownMenuProps {
    children?: any,
    forwardRef?: any,
    className?: string,
    open?: boolean,
    up?: boolean,
    right?: boolean,
    menuWidth?: string,
    menuHeight?: string,
    rows?: DropdownRowProps[]
}

export { DropdownMenuProps, DropdownRowProps };
