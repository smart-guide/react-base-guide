import React from "react";
import { DropdownRowProps } from "./interface";
import styled from "styled-components";

const Wrapper = styled.div<DropdownRowProps>`
  display: block;
  position: relative; 
  box-sizing: border-box;
  min-height: 34px;
  width: 100%;
  line-height: 34px;
  font-size: 14px;
  cursor: pointer;
  user-select: none;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  
  background: ${p => p.selected ? p.theme.palette.element.primary : p.theme.palette.element.background};
  color: ${p => p.selected ? p.theme.palette.text.contrast : p.theme.palette.text.primary};
  
  padding: 0 12px;
  border-radius: ${p => p.theme.radius.element};
  margin-bottom: 1px;
  
  &:hover {
    background: ${p => p.selected ? p.theme.palette.element.primary : p.theme.palette.background.secondary};
  }
   
  &.hovered {
    background: ${p => p.selected ? p.theme.palette.element.primary : p.theme.palette.background.secondary};
  }
`;

const Row = (props: DropdownRowProps) => (
    <Wrapper {...props} ref={props.forwardRef}>
        {props.value}
    </Wrapper>
);

export default Row;
