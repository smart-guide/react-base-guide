import React from "react";
import styled from "styled-components";
import Arrow from "../../icons/arrow";
import { DropdownButtonProps } from "./interface";
import getBorderColor from "@utils/get-border-color";

const ButtonStyled = styled.button<{ disabled: boolean }>`
  position: relative;
  height: 36px;
  width: 100%;
  outline: none;   
  cursor: pointer;
  user-select: none;
  padding: 0 35px 0 15px;
  border: 1px solid transparent;
  border-radius: ${p => p.theme.radius.element};
  background: ${p => p.disabled ? p.theme.palette.element.border : p.theme.palette.background.primary};
  color: ${p => p.theme.palette.text.primary};
  text-align: left;
  font-size: 14px;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  transition: border 0.3s;
  
  pointer-events: ${p => p.disabled ? "none" : "all"};

  & + fieldset {
    border: 1px solid ${p => getBorderColor(p)};
  }

  &:hover {
    & + fieldset {
      border: 1px solid ${p => getBorderColor({...p, hover: true})};
    }
  }

  &:focus {
    & + fieldset {
      border: 1px solid ${p => getBorderColor({...p, focus: true})};
      + label {
        color: ${p => getBorderColor({...p, focus: true})};
      }
    }
  }
`;

const StyledArrow = styled(Arrow)`
  display: block;
  position: absolute;
  pointer-events: none;
  top: 0;
  bottom: 0;
  right: 12px;
  margin: auto;
`;

const ButtonText = styled.span`
  pointer-events: none;
`;

const DropdownButton = (props: DropdownButtonProps) => (
    <ButtonStyled
        onClick={props.onClick}
        disabled={props.disabled}
        type="button"
    >
        <ButtonText>{props.children}</ButtonText>
        <StyledArrow rotate={props.open ? "-180deg" : "0"} />
    </ButtonStyled>
)

export default DropdownButton;
