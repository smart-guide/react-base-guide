import styled from "styled-components";
import { addCustomStyle, StyleProps } from "@utils/add-custom-style";

const DropdownRoot = styled.div<{ $styled: StyleProps }>`
  position: relative;
  display: inline-block;
  ${p => addCustomStyle(p.$styled)}
`;

export default DropdownRoot;
