import React, {useState} from "react";
import Menu from "@components/dropdown-menu";
import Label from "@components/label";
import dropdownMenuKeysHandler from "@utils/dropdown-menu-keys-handler";
import DropdownRoot from "./DropdownRoot";
import DropdownButton from "./DropdownButton";
import {DropdownProps} from "./interface";


const Dropdown = (props: DropdownProps) => {
    const [ open, setOpen ] = useState<boolean>(false);
    const selected = props.rows ? props.rows.filter(r => r.selected) : [];
    const text = selected[0] ? selected[0].value : "";

    return (
        <DropdownRoot
            onBlur={() => setOpen(false)}
            onKeyDown={(e) => dropdownMenuKeysHandler({ e, setOpen, rows: props.rows })}
            className={props.className}
            $styled={props.styled}
        >
            <Label
                focus={open}
                label={props.label}
                styled={{ display: "block", width: "100%" }}
            >
                <DropdownButton
                    onClick={() => setOpen(!open)}
                    open={open}
                    disabled={props.disabled}
                >{text}</DropdownButton>
            </Label>
            {!props.disabled && <Menu
                rows={props.rows}
                open={open}
                menuWidth={props.menuWidth || "100%"}
                menuHeight={props.menuHeight}
                right={props.right}
                up={props.up}
            />}
        </DropdownRoot>
    );
}

export default Dropdown;
