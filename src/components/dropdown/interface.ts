import {ButtonHTMLAttributes} from "react";
import { DropdownRowProps } from "../dropdown-menu/interface";
import { StyleProps } from "@utils/add-custom-style";

export interface DropdownProps extends ButtonHTMLAttributes<HTMLButtonElement> {
    label?: string;
    up?: boolean;
    right?: boolean;
    menuWidth?: string;
    menuHeight?: string;
    rows?: DropdownRowProps[];
    styled?: StyleProps;
}

export interface DropdownButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
    children?: any;
    open: boolean;
}
