import React from "react";
import styled from "styled-components";
import FlexProps from "./interface";
import { addCustomStyle, StyleProps } from "@utils/add-custom-style";

interface InternalFlexProps {
  $flex?: string | number
  $overflow?: string
  $flexDirection?: string
  $justifyContent?: string
  $alignItems?: string
  $flexWrap?: string
  $styled?: StyleProps
}

const propsMapper = (props: FlexProps): InternalFlexProps => {
    let newProps = {
        ...props,
        $flex: props.flex,
        $overflow: props.overflow,
        $flexDirection: props.flexDirection,
        $justifyContent: props.justifyContent,
        $alignItems: props.alignItems,
        $flexWrap: props.flexWrap,
        $styled: props.styled,
    }

    delete newProps.flex;
    delete newProps.overflow;
    delete newProps.flexDirection;
    delete newProps.justifyContent;
    delete newProps.alignItems;
    delete newProps.flexWrap;
    delete newProps.styled;

    return newProps;
};

const InternalFlex = styled.section<InternalFlexProps>`
  position: relative;
  box-sizing: border-box;
  overflow: ${p => p.$overflow};
  width: 100%;
  height: auto;
  
  flex: ${p => p.$flex || 1};
  flex-direction: ${p => p.$flexDirection || "row"};
  justify-content: ${p => p.$justifyContent || "flex-start"};
  align-items: ${p => p.$alignItems || "center"};
  flex-wrap: ${p => p.$flexWrap || "wrap"};
  
  ${p => addCustomStyle(p.$styled, "flex")}
`;

const Flex = (props: FlexProps) => (
    <InternalFlex {...propsMapper(props)} />
)



export default Flex;
