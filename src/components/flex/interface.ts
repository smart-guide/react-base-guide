import {HTMLProps} from "react";
import { StyleProps } from "@utils/add-custom-style";

interface FlexProps extends HTMLProps<HTMLDivElement> {
    flex?: string | number
    overflow?: "auto" | "hidden" | "scroll" | "visible" | "inherit"
    flexDirection?: "row" | "row-reverse" | "column" | "column-reverse"
    justifyContent?: "start" | "end" | "flex-start" | "flex-end" | "center" | "left" | "right" | "normal" | "baseline" | "first baseline" | "last baseline" | "space-between" | "space-around" | "space-evenly" | "stretch"
    alignItems?: "normal" | "flex-start" | "flex-end" | "center" | "baseline" | "stretch"
    flexWrap?: "nowrap" | "wrap" | "wrap-reverse"
    styled?: StyleProps
}

export default FlexProps;
