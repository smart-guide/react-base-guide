import React from "react";
import styled, { keyframes } from "styled-components";
import { addCustomStyle, StyleProps } from "@utils/add-custom-style";
import HintProps from "./interface";


const Down = keyframes`
  0% { transform: translateY(0); opacity: 0; }
  100% { transform: translateY(4px); opacity: 0.9; }
`;

const Up = keyframes`
  0% { transform: translateY(0); opacity: 0; }
  100% { transform: translateY(-4px); opacity: 0.9; }
`;

const Wrapper = styled.div<{ $styled: StyleProps, disabled: boolean, mobileHidden: boolean }>`
  position: relative;
  box-sizing: border-box;
  pointer-events: ${p => p.disabled ? "none" : "all"};
  ${p => addCustomStyle(p.$styled, "inline-block")}
  
  .hint-area {
    display: none;
  }
  
  &:hover {
    .hint-area {
      display: flex;

      @media(max-width: ${p => p.theme.screen.tablet}) {
        display: ${p => p.mobileHidden ? "none" : "flex"};
      }
    }
  }
`;

const HintArea = styled.div<HintProps>`
  position: absolute;
  box-sizing: border-box;
  min-width: ${p => p.hintWidth || "200px"};
  max-width: ${p => p.hintWidth || "200px"};
  right: ${p => p.right ? "0" : "auto"};
  bottom: ${p => p.up ? "100%" : "auto"};
  padding: 8px 12px;
  border-radius: ${p => p.theme.radius.element};
  box-shadow: ${p => p.theme.shadow.absolute};
  margin: 4px 0;
  z-index: ${p => p.theme.zIndex.dropdown};
  animation: ${p => p.up ? Up : Down} 0.3s forwards;
  
  background: ${p => p.theme.palette.background.contrast};
  color: ${p => p.theme.palette.text.contrast};
  font-size: 12px;
  line-height: 1.5;
  
  &::after {
    content: "";
    position: absolute;
    pointer-events: none;
    top: ${p => p.up ? "auto" : "-8px"};
    bottom: ${p => p.up ? "-8px" : "auto"};
    left: ${p => p.right ? "auto" : "8px"};
    right: ${p => p.right ? "8px" : "auto"};
    border: 8px solid transparent;
    border-bottom: ${p => p.up ? "0px" : "10px"} solid ${p => p.theme.palette.background.contrast};
    border-top: ${p => p.up ? "10px" : "0px"} solid ${p => p.theme.palette.background.contrast};
  }
`;

const Hint = (props: HintProps) => {
    const { children, hint } = props;

    if (!hint) {
        return children;
    }

    return (
        <Wrapper
            className={props.className}
            disabled={props.disabled}
            mobileHidden={props.mobileHidden}
            $styled={props.styled}
        >
            {children}

            <HintArea
                className="hint-area"
                hintWidth={props.hintWidth}
                up={props.up}
                right={props.right}
            >{hint}</HintArea>
        </Wrapper>
    );
}

export default Hint;
