import {HTMLProps, ReactElement} from "react";
import { StyleProps } from "@utils/add-custom-style";

interface HintProps extends HTMLProps<HTMLDivElement> {
    children?: any
    mobileHidden?: boolean
    disabled?: boolean

    up?: boolean
    right?: boolean
    hintWidth?: string
    hint?: ReactElement | ReactElement[] | string

    styled?: StyleProps
}

export default HintProps;
