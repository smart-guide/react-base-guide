import React from "react";
import styled from "styled-components";
import IconButtonProps from "./interface";
import Ripple from "../ripple";

const defaultButtonSize = "36px";
const defaultSvgSize = "24px";

const buttonSizes = {
    s: "28px",
    m: defaultButtonSize,
    l: "48px",
    xl: "64px",
    xxl: "96px",
}

const svgSizes = {
    s: "16px",
    m: defaultSvgSize,
    l: "32px",
    xl: "48px",
    xxl: "64px",
}

const ButtonRipple = styled(Ripple)<IconButtonProps>`
  display: ${p => p.styled?.display || "inline-block"};
  vertical-align: middle;
  width: ${p => buttonSizes[p.size] || defaultButtonSize};
  min-width: ${p => buttonSizes[p.size] || defaultButtonSize};
  height: ${p => buttonSizes[p.size] || defaultButtonSize};
  min-height: ${p => buttonSizes[p.size] || defaultButtonSize};
  border-radius: ${p => p.theme.radius.round};
  transition: box-shadow 0.3s;
  box-shadow: ${p => p.shadow && p.theme.shadow.element};
`;


const ButtonBase = styled.button<IconButtonProps>`
  display: flex;
  box-sizing: border-box;
  align-items: center;
  justify-content: space-around;
  outline: none;
  cursor: pointer;
  user-select: none;
  width: ${p => buttonSizes[p.size] || defaultButtonSize};
  min-width: ${p => buttonSizes[p.size] || defaultButtonSize};
  height: ${p => buttonSizes[p.size] || defaultButtonSize};
  min-height: ${p => buttonSizes[p.size] || defaultButtonSize};
  padding: 0;
  background: ${p => p.secondary ? "transparent" : p.theme.palette.element.primary};
  border: 1px solid transparent;
  border-radius: ${p => p.theme.radius.round};
  transition: background 0.3s;

  svg {
    width: ${p => svgSizes[p.size] || defaultSvgSize};
    min-width: ${p => svgSizes[p.size] || defaultSvgSize};
    height: ${p => svgSizes[p.size] || defaultSvgSize};
    min-height: ${p => svgSizes[p.size] || defaultSvgSize};
    pointer-events: none;
  }
  
  .rgb-fill {
    fill: ${p => p.secondary ? p.theme.palette.text.primary : p.theme.palette.text.contrast};
  }

  .rgb-stroke {
    stroke: ${p => p.secondary ? p.theme.palette.text.primary : p.theme.palette.text.contrast};
  }

  &:hover {
    background: ${p => p.secondary ? p.theme.palette.background.secondary :  p.theme.palette.element.primaryHover};
  }

  &:active {
    transform: translateY(1px);
  }
  
  &:disabled {
    pointer-events: none;
    opacity: 0.3;
  }
`;

const IconButton = (props: IconButtonProps) => (
    <ButtonRipple
        disabled={props.disabled}
        className={props.className}
        size={props.size}
        shadow={props.shadow}
        borderRadius="50%"
        styled={props.styled}
    >
        <ButtonBase
            {...props}
            className={undefined}
            styled={undefined}
        />
    </ButtonRipple>
)

export default IconButton;
