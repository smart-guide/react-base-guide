import {ButtonHTMLAttributes} from "react";
import { StyleProps } from "@utils/add-custom-style";

interface IconButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
    children?: any;
    secondary?: boolean;
    autoFocus?: boolean;
    size?: "s" | "m" | "l" | "xl" | "xxl";
    shadow?: boolean;
    styled?: StyleProps;
}

export default IconButtonProps;
