import InputProps from "./interface";
import {ChangeEventHandler} from "react";

const phoneRgxp = /[^0-9]/g;
const getPhoneValue = (value) => {
    if (!value) { return "" }
    value = value.replace(phoneRgxp,"");
    if (value.length === 0) {
        return "";
    }
    if (value.length > 1) {
        value = value.slice(0, 1) + " " + value.slice(1);
    }
    if (value.length > 5) {
        value = value.slice(0, 5) + " " + value.slice(5);
    }
    if (value.length > 9) {
        value = value.slice(0, 9) + "-" + value.slice(9);
    }
    if (value.length > 12) {
        value = value.slice(0, 12) + "-" + value.slice(12);
    }
    return "+" + value;
};

function phoneChange (e, onChange) {
    if (onChange) {
        e.target.value = getPhoneValue(e.target.value);
        onChange(e);
    }
}

const getNumberValue = (value, min, max) => {
    if (!isNaN(value) || value === "-") {
        if (min && +value < +min) { value = min; }
        if (max && +value > +max) { value = max; }
        return value;
    }
    return "";
}

function numberChange ({ e, onChange, min, max }) {
    if (onChange) {
        e.target.value = getNumberValue(e.target.value, min, max);
        onChange(e);
    }
}

const changeHandler = (props: InputProps): { value: string | number | readonly string[], onChange?: ChangeEventHandler } => {
    if (props.type === "tel") {
        return {
            value: props.value,
            onChange: (e) => phoneChange(e, props.onChange)
        };
    }

    if (props.type === "number") {
        return {
            value: props.value,
            onChange: (e) => numberChange({ e, onChange: props.onChange, min: props.min, max: props.max })
        }
    }
    return { value: props.value, onChange: props.onChange }
}

export default changeHandler;
