import React, {forwardRef, useState} from "react";
import Field from "./styled/Field";
import Message from "./styled/Message";
import {StyledClose, StyledPassword} from "./styled/StyledIcons";
import changeHandler from "./change-handler";
import InputProps from "./interface";
import Label from "@components/label";

const onClear = (e, props) => {
    e.preventDefault();
    props.onClear ? props.onClear() : (props.onChange && props.onChange({target: {value: ""}}));
}

const inputProps = (props: InputProps): InputProps => {
    let newProps = {...props};
    delete newProps.label;
    delete newProps.message;
    delete newProps.clear;
    delete newProps.onClear;
    delete newProps.styled;
    return newProps;
}

const Input = (props: InputProps, ref: any) => {
    const {value, onChange} = changeHandler(props);
    const [showPass, setShowPass] = useState<boolean>(false);
    const [focus, setFocus] = useState<boolean>(props.autoFocus || false);

    return (
        <Label
            label={props.label}
            focus={focus}
            tabIndex={props.tabIndex || 0}
            variant="outlined"
            error={props.error}
            warning={props.warning}
            success={props.success}
            styled={props.styled}
        >
            {props.type === "password" && <StyledPassword
                styled={{width: "14px", height: "38px"}}
                fill={showPass ? "active" : "secondary"}
                onClick={() => setShowPass(!showPass)}
            />}
            {(props.clear === true && props.value != "" && !props.disabled && props.type !== "password") && <StyledClose
                styled={{width: "10px", height: "38px"}}
                onClick={(e) => onClear(e, props)}
            />}

            {props.message &&
                <Message
                    $error={props.error}
                    $success={props.success}
                    $warning={props.warning}
                >{props.message}</Message>
            }

            <Field
                {...inputProps(props)}
                ref={ref}
                type={showPass ? "text" : props.type}
                placeholder={props.type === "tel" ? "+X XXX XXX-XX-XX" : props.placeholder}
                maxLength={props.type === "tel" ? 16 : props.maxLength}
                value={value}
                onChange={onChange}
                onFocus={e => {
                    setFocus(true);
                    props.onFocus && props.onFocus(e);
                }}
                onBlur={e => {
                    setFocus(false);
                    props.onBlur && props.onBlur(e);
                }}
            />
        </Label>
    )
};

export default forwardRef(Input);
