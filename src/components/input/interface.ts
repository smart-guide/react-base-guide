import {InputHTMLAttributes, ChangeEventHandler} from "react";
import {StyleProps} from "@utils/add-custom-style";

interface InputProps extends InputHTMLAttributes<HTMLInputElement> {
    rootRef?: any
    label?: string
    message?: string

    error?: boolean
    success?: boolean
    warning?: boolean

    clear?: boolean
    onClear?: ChangeEventHandler

    autoComplete?: "on" | "off"
    autoFocus?: boolean

    styled?: StyleProps
}

export default InputProps;
