import styled from "styled-components";
import getBorderColor from "@utils/get-border-color";
import InputProps from "../interface";


const Field = styled.input<InputProps>`
  display: block;
  position: relative;
  box-sizing: border-box;
  width: 100%;
  height: 100%;
  padding: 0 28px 0 16px;
  border-width: 0;
  border-radius: ${p => p.theme.radius.element};
  appearance: none;
  outline: none;
  box-shadow: none;
  background: transparent;
  
  font-size: 14px;
  transition: border 0.3s;
  color: ${p => p.theme.palette.text.primary};
  
  &::placeholder {
    color: ${p => p.theme.palette.text.placeholder};
  }

  &:-webkit-autofill {
    -webkit-background-clip: text;
  }
  
  -moz-appearance: textfield;
  &::-webkit-outer-spin-button,
  &::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }

  & + fieldset {
    border: 1px solid ${p => getBorderColor(p)};
  }
  
  &:hover {
    & + fieldset {
      border: 1px solid ${p => getBorderColor({...p, hover: true})};
    }
  }
  
  &:focus {
    & + fieldset {
      border: 1px solid ${p => getBorderColor({...p, focus: true})};
      + label {
        color: ${p => getBorderColor({...p, focus: true})};
      }
    }
  }

  &:disabled {
    background: ${p => p.theme.palette.element.disabled};
    border-color: ${p => p.theme.palette.element.disabled};
    color: ${p => p.theme.palette.text.disabled};
    pointer-events: none;
  }
`;

export default Field;
