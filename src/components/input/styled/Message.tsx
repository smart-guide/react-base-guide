import React from "react";
import styled, { keyframes } from "styled-components";

interface MessageProps {
    children?: any,
    $error?: boolean,
    $success?: boolean,
    $warning?: boolean,
}

const opacity = keyframes`
  0% { opacity: 0; }
  100% { opacity: 1; }
`;

const getColor = (p) => {
    if (p.$error) return p.theme.palette.element.error;
    if (p.$warning) return p.theme.palette.element.warning;
    if (p.$success) return p.theme.palette.element.success;
    return p.theme.palette.text.primary;
}

const Message = styled.div<MessageProps>`
  position: absolute;
  top: 100%;
  min-height: 15px;
  font-size: 12px;
  line-height: 1.2;
  color: ${p => getColor(p)};
  overflow: hidden;
  margin-top: 4px;
  margin-Left: 4px;
  animation: ${opacity} 0.5s;
  transition: color 0.3s;
`;

export default Message;
