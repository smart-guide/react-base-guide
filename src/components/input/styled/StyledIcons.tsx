import styled from "styled-components";
import Password from "../icons/PasswordIcon";
import CloseIcon from "../../../icons/close";

const StyledPassword = styled(Password)`
  position: absolute;
  z-index: ${p => p.theme.zIndex.relative};
  width: 16px;
  height: 36px;
  right: 10px;
  top: 0;
  bottom: 0;
  margin: auto;
`;

const StyledClose = styled(CloseIcon)`
  position: absolute;
  z-index: ${p => p.theme.zIndex.relative};
  right: 12px;
  top: 0;
  bottom: 0;
  margin: auto;
`;

export { StyledPassword, StyledClose };
