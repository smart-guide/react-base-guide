import React from "react";
import styled from "styled-components";
import { Variant } from "./interface";

interface FieldsetProps {
    error: boolean;
    warning: boolean;
    success: boolean;
    height: string;
    minHeight: string;
    variant: Variant;
    hidden: boolean;
    label: any;
}

interface FieldsetRootProps {
    $error: boolean;
    $warning: boolean;
    $success: boolean;
    $height: string;
    $minHeight: string;
}

interface LegendProps {
    $variant: Variant;
    $hidden: boolean;
}


const FieldsetRoot = styled.fieldset<FieldsetRootProps>`
  position: absolute;
  box-sizing: border-box;
  pointer-events: none;
  overflow: hidden;
  width: 100%;
  height: ${p => p.$height};
  min-height: ${p => p.$minHeight ? (+p.$minHeight.replace("px", "") + 7.5) + "px" : "0px"};
  top: 0;
  left: 0;
  padding: 0;
  margin: 0;
  inset: -5.5px 0 0;
  border-radius: ${p => p.theme.radius.element};
  transition: border 0.3s;
`;

const Legend = styled.legend<LegendProps>`
  display: block;
  float: unset;
  overflow: hidden;
  white-space: nowrap;
  visibility: hidden;
  height: 12px;
  width: auto;
  max-width: ${p => (p.$hidden || p.$variant === "text") ? "0px" : "100%"};
  padding: ${p => (p.$hidden || p.$variant === "text") ? "0px" : "0 6px"};
  margin-left: 9px;
  font-size: 12px;
  font-weight: 400;
  line-height: 1.2;
  text-align: start;
  transition: max-width 0.3s;
`;

const Fieldset = (props: FieldsetProps) => (
    <FieldsetRoot
        $error={props.error}
        $warning={props.warning}
        $success={props.success}
        $height={props.height}
        $minHeight={props.minHeight}
    >
        <Legend $variant={props.variant} $hidden={!props.label}>{props.label}</Legend>
    </FieldsetRoot>
);

export default Fieldset;
