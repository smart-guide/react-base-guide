import styled from "styled-components";
import getBorderColor from "@utils/get-border-color";
import { addCustomStyle, StyleProps } from "@utils/add-custom-style";
import {Variant} from "./interface";

interface LabelRootProps {
    $variant: Variant;
    $label: string;
    $focus: boolean;
    $error: boolean;
    $warning: boolean;
    $success: boolean;
    $styled: StyleProps;
}

const LabelRoot = styled.div<LabelRootProps>`
  position: relative;
  box-sizing: border-box;
  min-height: ${p => p.$variant === "standard" ? "0px" : "38px"};
  height: ${p => p.$variant === "standard" ? "auto" : "38px"};
  ${p => addCustomStyle({...p.$styled, height: p.$styled?.height ? (+p.$styled?.height?.replace("px", "") - 6) + "px" : undefined}, "inline-block")}

  input, textarea, button {
    &::placeholder {
      color: ${p => p.$label && p.$variant !== "standard" ? "transparent" : undefined};
    }
    
    & + fieldset {
      border: 1px solid ${p => getBorderColor(p)};
    }

    &:hover {
      & + fieldset {
        border: 1px solid ${p => getBorderColor({...p, hover: true})};
      }
    }

    &:focus {
      & + fieldset {
        border: 1px solid ${p => getBorderColor({...p, focus: true})};
        + label {
          color: ${p => getBorderColor({...p, focus: true})};
        }
      }
    }
  }
`;

export default LabelRoot
