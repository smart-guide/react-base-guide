import styled from "styled-components";
import { Variant } from "./interface";

interface TextProps {
    $variant: Variant;
}

const Text = styled.label<TextProps>`
  position: ${p => p.$variant === "standard" ? "relative" : "absolute"};
  pointer-events: ${p => p.$variant === "standard" ? "all" : "none"};
  box-sizing: border-box;
  cursor: pointer;
  user-select: none;
  left: ${p => p.$variant === "standard" ? "0px" : "12px"};
  top: ${p => p.$variant === "standard" ? "0px" : p.$variant === "text" ? "9px" : "-8px"};
  padding: 2px;
  font-size: ${p => p.$variant === "text" ? "14px" : "12px"};
  font-weight: 400;
  line-height: 1.2;
  color: ${p => p.theme.palette.text.secondary};
  z-index: ${p => p.theme.zIndex.relative};
  transition: left 0.3s, top 0.3s, font-size 0.3s, color 0.3s;

  &:first-letter {
    text-transform: uppercase;
  }
`;

export default Text;
