import React, { useState, useCallback } from "react";
import LabelRoot from "./LabelRoot";
import Fieldset from "./Fieldset";
import Text from "./Text";
import { LabelProps, Variant } from "./interface";


const Label = (props: LabelProps) => {
    const [variant, setVariant] = useState<Variant>(props.variant === "standard" ? props.variant : "outlined");

    const updateVariant = (value) => {
        const newPosition = ((value === "" || value === undefined || value === null) && props.focus === false) ? "text" : (props.variant || "outlined");
        setVariant(newPosition)
    }

    const labelRef = useCallback(node => {
        if (node) {
            const child = node.querySelector("input, textarea");
            if (child) {
                const value = child?.value;
                updateVariant(value);
            }
            return node;
        }
    }, [props.focus, props.variant]);


    if (props.variant === "standard") {
        return (
            <LabelRoot
                className={props.labelClassName}
                $focus={props.focus}
                $variant={variant}
                $label={props.label}
                $error={props.error}
                $warning={props.warning}
                $success={props.success}
                $styled={props.styled}
            >
                <Text $variant={variant} className={props.className}>
                    {props.label}
                    {props.children}
                </Text>
            </LabelRoot>
        )
    }

    return (
        <LabelRoot
            ref={labelRef}
            className={props.labelClassName}
            $focus={props.focus}
            $variant={variant}
            $label={props.label}
            $error={props.error}
            $warning={props.warning}
            $success={props.success}
            $styled={props.styled}
        >
            {props.children}

            <Fieldset
                error={props.error}
                warning={props.warning}
                success={props.success}
                height={props.styled?.height}
                minHeight={props.styled?.minHeight}
                hidden={props.hidden}
                variant={variant}
                label={props.label}
            />

            {props.label && <Text $variant={variant}>
                {props.label}
            </Text>}
        </LabelRoot>
    )
};

export default Label;
