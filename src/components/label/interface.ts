import {LabelHTMLAttributes} from "react";
import {StyleProps} from "@utils/add-custom-style";

export type Variant = "text" | "outlined" | "standard";

export interface LabelProps extends LabelHTMLAttributes<HTMLLabelElement> {
    label?: any;
    labelClassName?: string;

    error?: boolean;
    warning?: boolean;
    success?: boolean;

    disabled?: boolean;
    focus?: boolean;
    variant?: Variant;
    styled?: StyleProps;
}


export default LabelProps;
