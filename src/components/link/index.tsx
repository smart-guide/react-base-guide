import React from "react";
import styled from "styled-components";
import { Link as RouterLink, LinkProps as RouterLinkProps } from "react-router-dom";
import LinkProps from "./interface";
import addCustomStyle from "@utils/add-custom-style";

const scProps = (props: LinkProps): LinkProps => {
    let newProps = {...props};
    delete newProps.styled;
    delete newProps.variant;
    delete newProps.color;
    delete newProps.disabled;
    return newProps;
}

const Link = styled((props: LinkProps) => props.to ? <RouterLink {...scProps(props) as RouterLinkProps} /> : <a {...scProps(props)} />)`
  display: inline-block;
  position: relative;
  text-decoration: none;
  cursor: pointer;
  user-select: none;
  color: ${p => p.theme.palette.text[p.color] || p.theme.palette.text.active};
  font-size: 14px;
  line-height: 1.5;
  transition: background 0.3s;
  opacity: ${p => p.disabled ? "0.5" : 1};
  pointer-events: ${p => p.disabled ? "none" : "all"};
  
  ${p => addCustomStyle(p.styled)}

  &::after {
    content: "";
    display: ${p => p.variant === "simple" ? "none" : "block"};
    position: absolute;
    width: ${p => p.variant === "underline" ? "100%" : "0"};
    height: 1px;
    left: 0;
    background: ${p => p.theme.palette.text[p.color] || p.theme.palette.text.active};
    opacity: 0.6;
    transition: width 0.3s, opacity 0.3s;
  }

  &:hover {
    &::after {
      width: 100%;
      ${p => p.variant === "underline" && "opacity: 1;"}
    }
  }
`;

export default Link;
