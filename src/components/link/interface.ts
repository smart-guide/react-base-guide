import { LinkProps as RouterLinkProps } from "react-router-dom";
import { StyleProps } from "@utils/add-custom-style";

interface LinkProps extends Omit<RouterLinkProps, "to"> {
    to?: string
    href?: string
    disabled?: boolean
    variant?: "simple" | "underline"
    color?: "primary" | "error" | "warning" | "success" | "contrast" | "active"
    styled?: StyleProps
}

export default LinkProps;
