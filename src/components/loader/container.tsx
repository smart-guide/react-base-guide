import styled from "styled-components";

const LoaderContainer = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
  margin: auto;
  user-select: none;
`;

export default LoaderContainer;
