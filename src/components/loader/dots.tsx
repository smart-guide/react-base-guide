import React from "react";
import styled, { keyframes } from "styled-components";

const animateWidth = 64;
const size = 16;

const keyframe1 = keyframes`
  0% { transform: translate3d(0, 0, 0); }
  60% { transform: translate3d(${animateWidth + "px"}, 0, 0); }
  70% { transform: translate3d(${animateWidth + "px"}, 0, 0); }
  100% { transform: translate3d(0, 0, 0); }
`;

const keyframe2 = keyframes`
  0% { transform: translate3d(0, 0, 0); }
  20% { transform: translate3d(0, 0, 0); }
  60% { transform: translate3d(${animateWidth + "px"}, 0, 0); }
  80% { transform: translate3d(${animateWidth + "px"}, 0, 0); }
  100% { transform: translate3d(0, 0, 0); }
`;

const keyframe3 = keyframes`
  0% { transform: translate3d(0, 0, 0); }
  40% { transform: translate3d(0, 0, 0); }
  60% { transform: translate3d(${animateWidth + "px"}, 0, 0); }
  90% { transform: translate3d(${animateWidth + "px"}, 0, 0); }
  100% { transform: translate3d(0, 0, 0); }
`;

const DotsContainer = styled.div`
  position: relative;
  width: ${animateWidth + size}px;
  height: 100%;
  filter: url(#loader);
  margin: auto;
`;

const Dot = styled.div`
  position: absolute;
  display: block;
  width: ${size}px;
  height: ${size}px;
  top: 0;
  bottom: 0;
  left: 0;
  background: ${p => p.theme.palette.element.primary};
  border-radius: ${p => p.theme.radius.round};
  margin: auto;
  transform: translate3d(0, 0, 0);

  &:nth-child(1) {
    animation: ${keyframe1} 2s ease-in-out infinite;
  }
  &:nth-child(2) {
    animation: ${keyframe2} 2s ease-in-out infinite;
  }
  &:nth-child(3) {
    animation: ${keyframe3} 2s ease-in-out infinite;
  }
`;


const Dots = () => (
    <DotsContainer>
        <Dot />
        <Dot />
        <Dot />
    </DotsContainer>
);

export default Dots;
