import React from "react";
import Container from "./container";
import Svg from "./svg";
import Dots from "./dots";

const Loader = () => (
    <Container>
        <Svg />
        <Dots />
    </Container>
);

export default Loader;
