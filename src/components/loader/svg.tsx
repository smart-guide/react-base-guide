import React from "react";
import styled from "styled-components";

const StyledSvg = styled.svg`
  position: absolute;
  width: 100%;
  height: 100%;
  left: 0;
  top: 0;
`;

const Svg = () => (
    <StyledSvg>
        <defs>
            <filter id="loader">
                <feGaussianBlur stdDeviation="5" result="blur"/>
                <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 18 -7" result="loader"/>
                <feBlend in2="loader"/>
            </filter>
        </defs>
    </StyledSvg>
);

export default Svg;
