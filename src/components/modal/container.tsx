import styled from "styled-components";

const ModalContainer = styled.div`
  position: fixed;
  display: flex;
  align-items: center;
  width: 100vw;
  height: 100vh;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
  z-index: ${p => p.theme.zIndex.modal};
  overflow: hidden;
  filter: none;
`;

export default ModalContainer;
