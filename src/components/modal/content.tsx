import styled from "styled-components";

const ModalContent = styled.div<{ layer?: boolean }>`
  position: relative;
  max-height: ${p => p.layer ? "100%" : "calc(100vh - 160px)"};
  overflow: auto;
  align-items: start;
  padding: 24px;
  margin: -24px;
  
  @media(max-width: ${p => p.theme.screen.tablet}) {
    max-height: ${p => p.layer ? "100%" : "calc(100vh - 80px)"};
    padding: 10px;
    margin: -10px;
  }
`;

export default ModalContent;
