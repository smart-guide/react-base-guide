import React, { useRef, useState, useEffect } from "react";
import Layer from "./layer";
import Window from "./window";
import Title from "./title";
import Content from "./content";
import StyledClose from "./styled-close";
import ModalContainer from "./container";
import { ModalProps } from "./interface";

const Modal = (props: ModalProps) => {
    const [visible, setVisible] = useState<boolean>(props.open);
    const [animation, setAnimation] = useState<"show"|"hide">(null);
    const modalLayer = useRef(null);

    useEffect(() => {
        if (props.open) {
            setVisible(true);
            setAnimation("show");
            document.body.style.overflow = "hidden";
            document.onkeydown = (e) => {
                if (e.key === "Escape") { props.onClose(e as any); }
            };
        } else {
            setAnimation("hide");

            setTimeout(() => {
                document.body.style.overflow = "auto";
                document.onkeydown = null;
                setAnimation(null);
                setVisible(false);
            }, 300);
        }

        return function () {
            document.body.style.overflow = "auto";
            document.onkeydown = null;
        }
    }, [props.open]);

    if (!visible) { return null; }

    return (
        <ModalContainer
            id={props.id}
            ref={props.forwardRef}
        >
            <Layer
                ref={modalLayer}
                onClick={props.onClose}
                id={"modal-layer-" + props.id}
            />
            <Window
                className={props.className}
                animation={animation}
                layer={props.layer}
                maxWidth={props.maxWidth}
                maxHeight={props.maxHeight}
            >
                {props.title && <Title>{props.title}</Title>}

                <Content layer={props.layer}>
                    {props.children}
                </Content>

                {(!props.layer && !props.hideClose) && <StyledClose onClick={props.onClose} />}
            </Window>
        </ModalContainer>
    );
}

export default Modal;
