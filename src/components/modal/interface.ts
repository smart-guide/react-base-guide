import {KeyboardEventHandler, MouseEventHandler} from "react";

interface ModalProps {
    // react
    ref?: any,
    forwardRef?: any,
    children?: any,

    // default
    id?: string,
    name?: string,
    className?: string,

    // styled
    maxWidth?: string,
    maxHeight?: string,

    // states
    layer?: boolean,
    hideClose?: boolean,

    // content
    title?: any,
    open: boolean
    onClose: MouseEventHandler
}

interface ModalWindowProps {
    // states
    children?: any,
    animation: "show"|"hide",
    layer?: boolean,

    // styled
    className?: string,
    maxWidth?: string,
    maxHeight?: string,
}

export { ModalProps, ModalWindowProps };
