import styled from "styled-components";

const ModalLayer = styled.div`
  position: absolute;
  box-sizing: border-box;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
  overflow: auto;
  z-index: ${p => (p.theme.zIndex.modal - 1)};
  background: rgba(0, 0, 0, 0.7);
  perspective: 1000px;
`;

export default ModalLayer;
