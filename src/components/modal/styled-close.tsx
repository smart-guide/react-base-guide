import styled from "styled-components";
import Close from "../../icons/close";

const StyledClose = styled(Close)`
  position: absolute;
  top: 32px;
  right: 32px;
  z-index: ${p => p.theme.zIndex.modal};
  transition: opacity 0.3s, transform 0.3s;
  &:hover {
    transform: rotate(90deg);
  }
  
  @media(max-width: ${p => p.theme.screen.tablet}) {
    top: 10px;
    right: 10px;
  }
`;

export default StyledClose;
