import styled from "styled-components";

const ModalTitle = styled.div`
  display: block;
  position: relative;
  font-size: ${p => p.theme.fontSize.primaryTitle};
  line-height: 1;
  text-align: start;
  background: ${p => p.theme.palette.background.primary};
  color: ${p => p.theme.palette.text.primary};
  z-index: ${p => p.theme.zIndex.relative};
  margin-bottom: 24px;
`;

export default ModalTitle;
