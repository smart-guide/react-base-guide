import React from "react";
import styled, { keyframes } from "styled-components";
import { ModalWindowProps } from "./interface";

const show = keyframes`
  0% { transform: scale(0.3); opacity: 0; }
  100% { transform: scale(1); opacity: 1; }
`;

const hide = keyframes`
  0% { transform: scale(1); opacity: 1; }
  100% { transform: scale(0); opacity: 0; }
`;

const showLayer = keyframes`
  0% { transform: translateX(-100%); opacity: 1; }
  100% { transform: translateX(0); opacity: 1; }
`;

const hideLayer = keyframes`
  0% { transform: translateX(0); opacity: 1; }
  100% { transform: translateX(-100%); opacity: 1; }
`;

const getStyles = (p) => {
    return p.layer ? `
        width: auto;
        max-width: 70%;
        height: 100%;
        margin: 0;
    ` : `
        width: 100%;
        max-width: ${p.maxWidth || p.theme.screen.tablet};
        height: auto;
        margin: 0 auto;
        border-radius: ${p.theme.radius.element};
    `;
};

const animationsJson = {
    show,
    hide
}

const animationLayerJson = {
    show: showLayer,
    hide: hideLayer
}

const Window = styled.div<ModalWindowProps>`
  position: relative;
  box-sizing: border-box;
  flex: none;
  min-height: 100px;
  max-height: ${p => p.maxHeight};
  padding: 24px;
  background: ${p => p.theme.palette.background.primary};
  z-index: ${p => p.theme.zIndex.modal};
  opacity: 0;

  @media(max-width: ${p => p.theme.screen.tablet}) {
    max-width: calc(100% - 30px);
    padding: 10px;
  }
    
  ${p => getStyles(p)}
  animation: 0.3s ${p => (p.layer ? animationLayerJson[p.animation] : animationsJson[p.animation]) || "none"} forwards;
`;

export default Window;
