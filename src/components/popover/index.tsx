import React, { useState, useRef } from "react";
import styled from "styled-components";
import { addCustomStyle, StyleProps } from "@utils/add-custom-style";
import IPopover from "./interface";


const Wrapper = styled.div<{ $styled: StyleProps }>`
  position: relative;
  box-sizing: border-box;
  outline: none;
  user-select: none;
  
  ${p => addCustomStyle(p.$styled, "inline-block")}
`;

const PopoverHead = styled.div`
  position: relative;
  box-sizing: border-box;
  cursor: pointer;
  z-index: ${p => p.theme.zIndex.relative};
`;

const Body = styled.div<{ right: boolean, up: boolean }>`
  position: absolute;
  box-sizing: border-box;
  right: ${p => p.right ? "0" : "auto"};
  bottom: ${p => p.up ? "100%" : "auto"};
  z-index: ${p => p.theme.zIndex.absolute};
`;

const onBlur = (e, setOpen, containerRef) => {
    if (!e.relatedTarget || !containerRef.current.contains(e.relatedTarget)) {
        setOpen(false);
    }
}

const Popover = (props: IPopover) => {
    const [open ,setOpen] = useState(props.open || false);
    const containerRef = useRef(null);

    return (
        <Wrapper
            ref={containerRef}
            tabIndex={0}
            onBlur={e => onBlur(e, setOpen, containerRef)}
            $styled={props.styled}
        >
            <PopoverHead
                className={open ? "popover-open" : "popover-close"}
                onClick={(e) => {
                    e.preventDefault();
                    e.stopPropagation();
                    setOpen(!open);
                }}
            >
                {props.head}
            </PopoverHead>

            {open && <Body up={props.up} right={props.right}>{props.children}</Body>}
        </Wrapper>
    );
}

export default Popover;
