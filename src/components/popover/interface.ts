import {ButtonHTMLAttributes} from "react";
import { StyleProps } from "@utils/add-custom-style";

interface PopoverProps extends ButtonHTMLAttributes<HTMLButtonElement> {
    children?: any
    head?: any
    open?: boolean
    up?: boolean
    right?: boolean
    styled?: StyleProps
}

export default PopoverProps;
