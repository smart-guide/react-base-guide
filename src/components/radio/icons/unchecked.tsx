import React from "react";
import { Svg, Circle, IconProps } from "@components/svg";

const RadioUnchecked = (props: IconProps) =>
    <Svg
        {...props}
        width="18px"
        height="18px"
        verticalAlign="-5px"
        cursor="pointer"
        styled={{ marginRight: "8px" }}
        viewBox="0 0 18 18"
    >
        <g stroke="none" fill="none">
            <g transform="translate(-39.000000, -448.000000)">
                <g transform="translate(40.000000, 449.000000)">
                    <Circle
                        cx="8"
                        cy="8"
                        r="8"
                        stroke="secondary"
                        strokeWidth="1px"
                        className="rgb-circle-hovered"
                    />
                    <Circle
                        cx="8"
                        cy="8"
                        r="4"
                    />
                </g>
            </g>
        </g>
    </Svg>;

export default RadioUnchecked;
