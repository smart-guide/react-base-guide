import React from "react";
import styled from "styled-components";
import Label from "@components/label";
import CheckedIcon from "./icons/checked";
import UncheckedIcon from "./icons/unchecked";
import RadioProps from "./interface";


const RadioInput = styled.input`
  display: none;
`;

const RadioLabel = styled(Label)`
  opacity: ${p => p.disabled ? "0.7" : "1"};
  pointer-events: ${p => p.disabled ? "none": "all"};
  
  &:hover {
    color: ${p => p.theme.palette.element.primary};

    .rgb-circle-hovered {
      stroke: ${p => p.theme.palette.element.primary};
    }
  }
`;

const Radio = (props: RadioProps) => (
    <RadioLabel
        variant="standard"
        disabled={props.disabled}
        className={props.className}
        styled={props.styled}
        label={
            <>
                {props.checked ? <CheckedIcon disabled={props.disabled} /> : <UncheckedIcon disabled={props.disabled} />}
                {props.label}
            </>
        }
    >
        <RadioInput
            type="radio"
            disabled={props.disabled}
            checked={props.checked || false}
            onChange={props.onChange}
        />
    </RadioLabel>
);
export default Radio;
