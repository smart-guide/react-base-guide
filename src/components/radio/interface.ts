import {InputHTMLAttributes, ReactElement} from "react";
import { StyleProps } from "@utils/add-custom-style";

interface RadioProps extends InputHTMLAttributes<HTMLInputElement> {
    label?: ReactElement | string
    checked?: boolean
    styled?: StyleProps
}

export default RadioProps;
