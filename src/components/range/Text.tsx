import styled from "styled-components";

const Text = styled.label`
  position: relative;
  display: inline-block;
  box-sizing: border-box;
  font-size: 12px;
  font-weight: 400;
  line-height: 1;
  color: ${p => p.theme.palette.text.secondary};
  margin-left: 4px;
`;

export default Text;
