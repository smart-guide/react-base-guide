import styled from "styled-components";
import { addCustomStyle, StyleProps } from "@utils/add-custom-style";

const RangeRoot = styled.div<{ $styled: StyleProps }>`
  align-items: center;
  position: relative;
  box-sizing: border-box;
  width: 100%;
  flex-wrap: nowrap;
  ${p => addCustomStyle(p.$styled, "flex")}
`;

const Container = styled.div`
  position: relative;
  display: block;
  box-sizing: border-box;
  width: 100%;
`;

export { RangeRoot, Container };
