import React from "react";
import { RangeRoot, Container } from "./container";
import InputRange from "./input";
import Progress from "./progress";
import Thumb from "./thumb";
import Text from "./Text";
import { RangeProps } from "./interface";

const getProgressWidth = ({ min, max, value }: RangeProps) => {
    let numbersInRange = +max - +min;
    let valueInRange = +value - +min;
    let percent = (100 * valueInRange) / numbersInRange;

    if (percent > 100) { return "100%"; }
    if (percent <= 0) { return 0; }
    return percent + "%";
};

const Range = (props: RangeProps) => {
    const percent = getProgressWidth(props);

    return (
        <RangeRoot
            className={props.className}
            $styled={props.styled}
        >
            <Container>
                <InputRange
                    value={props.value}
                    onChange={props.onChange}
                    disabled={props.disabled}
                    min={props.min}
                    max={props.max}
                    step={props.step || 1}
                    type="range"
                />

                <Progress width={percent} disabled={props.disabled} />
                <Thumb left={percent} disabled={props.disabled} />
            </Container>

            {props.count && <Text>{props.value ? props.value.toString() : "0"}</Text>}
        </RangeRoot>
    );
};

export default Range;
