import styled from "styled-components";

const track = p => `
    box-sizing: border-box;
    -webkit-appearance: none;
    height: 8px;
    border-radius: ${p.theme.radius.element};
    background: ${p.theme.palette.element.border};
`;

const InputRange = styled.input`
  display: block;
  position: relative;
  box-sizing: border-box;
  cursor: pointer;
  -webkit-appearance: none;
  outline: none;
  width: 100%;
  height: 28px;
  background: transparent;
  margin: 0;
  
  &:disabled {
    pointer-events: none;
  }
  
  &::-webkit-slider-runnable-track {
    ${p => track(p)};
  }
  &::-moz-range-track {
    ${p => track(p)};
  }
  &::-ms-track {
    ${p => track(p)};
  }
  &::-ms-fill-lower {
    ${p => track(p)};
    background: ${p => p.theme.palette.element.primary};
  }
  
  
  &::-webkit-slider-thumb {
    opacity: 0;
  }
  &::-moz-range-thumb {
    opacity: 0;
  }
  &::-ms-thumb {
    opacity: 0;
  }
`;

export default InputRange;
