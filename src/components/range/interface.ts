import {InputHTMLAttributes} from "react";
import { StyleProps } from "@utils/add-custom-style";

interface RangeProps extends InputHTMLAttributes<HTMLInputElement> {
    value?: number,
    min?: number,
    max?: number,
    step?: number,
    count?: boolean,
    styled?: StyleProps
}

interface ThumbProps {
    left?: string | number,
    disabled?: boolean
}

export { RangeProps, ThumbProps };
