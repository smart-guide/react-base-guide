import styled from "styled-components";

const Progress = styled.div<{ disabled: boolean, width: string | 0 }>`
  display: block;
  position: absolute;
  pointer-events: none;
  max-width: 100%;
  
  height: 8px;
  top: 10px;
  border-radius: ${p => p.theme.radius.element};
  z-index: ${p => p.theme.zIndex.relative};
  background: ${p => p.disabled ? p.theme.palette.element.disabled : p.theme.palette.element.primary};
  width: ${p => p.width};
`;

export default Progress;
