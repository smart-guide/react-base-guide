import React from "react";
import styled from "styled-components";
import { ThumbProps } from "./interface";

const getMargin = (left) => {
    if (left === 0) {
        return 0;
    }
    if (left === "100%") {
        return "-15px";
    }
    return "-8px;"
}

const Thumb = styled.div<ThumbProps>`
  position: absolute;
  box-sizing: border-box;
  -webkit-appearance: none;
  pointer-events: none;
  width: 16px;
  left: ${p => (p.left) || "0"};
  top: 6px;
  height: 16px;
  border-radius: ${p => p.theme.radius.round};
  border: none;
  background: ${p => p.disabled ? p.theme.palette.element.disabled : p.theme.palette.element.primary};
  box-shadow: ${p => p.theme.shadow.element};
  margin-left: ${p => getMargin(p.left)};
  z-index: ${p => p.theme.zIndex.relative};
`;

export default Thumb;
