import React, {ReactElement} from "react";
import Ripples from "react-ripples";
import styled from "styled-components";
import { addCustomStyle, StyleProps } from "@utils/add-custom-style";

interface BaseRippleProps {
  className?: string
  disabled: boolean
  children?: ReactElement
  borderRadius?: string
  $styled?: StyleProps
}

const BaseRipple = styled((props: BaseRippleProps) => <Ripples className={props.className}>{props.children}</Ripples>)`
  pointer-events: ${p => p.disabled ? "none" : "all"};
  border-radius: ${p => p.borderRadius || p.theme.radius.element};
  ${p => addCustomStyle(p.$styled)}
`;

export default BaseRipple;
