import React from "react";
import BaseRipple from "./base";
import RippleProps from "./interface";

const Ripple = (props: RippleProps) => (
    <BaseRipple
        disabled={props.disabled}
        className={props.className}
        borderRadius={props.borderRadius}
        $styled={props.styled}
    >{props.children}</BaseRipple>
)

export default Ripple;
