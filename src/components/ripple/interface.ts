import {ReactElement} from "react";
import { StyleProps } from "@utils/add-custom-style";

interface RippleProps {
    className?: string
    disabled?: boolean
    children?: ReactElement
    borderRadius?: string
    styled?: StyleProps
}

export default RippleProps;
