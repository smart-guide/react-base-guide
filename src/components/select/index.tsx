import React, { useState, useRef } from "react";
import Label from "@components/label";
import Menu from "@components/dropdown-menu";
import Wrapper from "./wrapper";
import Row from "./row";
import Selected from "./selected";
import { SelectProps } from "./interface";

const scrollToHovered = (menu) => {
    let hovered = menu.current && menu.current.querySelector(".hovered");
    if (hovered && menu.current) {
        hovered && (menu.current.scrollTop = (hovered.offsetTop - (hovered.offsetHeight + 4)));
    }
}

const selectByKey = ({ e, rows, hoverIndex, setOpen, setHoverIndex, menu }) => {
    e.preventDefault();

    switch (e.key) {
        case "Enter": {
            rows[hoverIndex].onClick();
            return;
        }
        case "Escape": {
            setOpen(false)
            return;
        }
        case "ArrowDown":
        case "ArrowUp": {
            let newHoverIndex = e.key === "ArrowDown" ? hoverIndex + 1 : hoverIndex - 1;
            if (newHoverIndex < 0) { newHoverIndex = 0 }
            if (newHoverIndex >= rows.length - 1) { newHoverIndex = rows.length - 1 }
            if (hoverIndex !== newHoverIndex) {
                setHoverIndex(newHoverIndex)
                scrollToHovered(menu);
            }
            return;
        }
    }
}

const onBlur = (e, setOpen, containerRef) => {
    if (!e.relatedTarget || !containerRef.current.contains(e.relatedTarget)) {
        setOpen(false);
    }
}

const Select = (props: SelectProps) => {
    const [ open, setOpen ] = useState(false);
    const [ hoverIndex, setHoverIndex ] = useState(0);
    const containerRef = useRef(null);
    const menu = useRef(null);

    return (
        <Label
            variant="standard"
            label={props.label}
            className={props.className}
            styled={props.styled}
        >
            <Wrapper
                ref={containerRef}
                tabIndex={props.tabIndex || 0}
                onClick={() => props.disabled ? null : setOpen(true)}
                onKeyDown={(e) => selectByKey({ e, rows: props.rows, hoverIndex, setHoverIndex, setOpen, menu })}
                onBlur={e => onBlur(e, setOpen, containerRef)}
            >
                <Selected rows={props.rows} disabled={props.disabled} />

                <Menu
                    forwardRef={menu}
                    open={open}
                    menuWidth={props.menuWidth || "100%"}
                    menuHeight={props.menuHeight}
                    right={props.right}
                    up={props.up}
                >
                    {props.rows && props.rows.map((row, key) => (
                        <Row
                            key={key}
                            selected={row.selected}
                            hovered={key === hoverIndex}
                            onClick={row.onClick}
                            onMouseEnter={() => setHoverIndex(key)}
                            value={row.value}
                        />
                    ))}
                </Menu>
            </Wrapper>
        </Label>
    );
}

export default Select;
