import {ButtonHTMLAttributes, MouseEventHandler} from "react";
import { StyleProps } from "@utils/add-custom-style";

interface SelectRowProps {
    key?: any,
    forwardRef?: any,
    value: string,
    selected?: boolean,
    hovered?: boolean,
    onClick?: MouseEventHandler
    onMouseEnter?: MouseEventHandler
}


interface SelectProps extends ButtonHTMLAttributes<HTMLButtonElement>{
    label?: string,
    up?: boolean,
    right?: boolean,
    menuWidth?: string,
    menuHeight?: string,
    rows?: SelectRowProps[]
    styled?: StyleProps
}


interface SelectInputProps {
    rows?: SelectRowProps[],
    disabled: boolean
}


export { SelectProps, SelectRowProps, SelectInputProps }
