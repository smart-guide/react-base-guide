import React from "react";
import styled from "styled-components";
import Checkbox from "@components/checkbox";

import { SelectRowProps } from "./interface";

const getBackground = p => {
    if (p.$selected) {
        return `background: ${p.theme.palette.element.primary}`;
    }
    if (p.$hovered) {
        return `background: ${p.theme.palette.background.secondary}`;
    }
    return `background: ${p.theme.palette.background.primary}`;
}

const Container = styled.div<{ $selected?: boolean, $hovered: boolean }>`
  display: flex;
  align-items: center;
  position: relative;
  box-sizing: border-box;
  width: 100%;
  height: 34px;
  line-height: 34px;
  font-size: 14px;
  cursor: pointer;
  user-select: none;
  padding: 0 12px;
  border-radius: ${p => p.theme.radius.element};
  margin-bottom: 1px;
  ${p => getBackground(p)};
  
  &:hover {
    background: ${p => p.$selected ? p.theme.palette.element.primary : p.theme.palette.background.secondary};
  }
`;

const Text = styled.div<{ selected?: boolean }>`
  display: inline-block;
  vertical-align: middle;
  max-width: 100%;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
  color: ${p => p.selected ? p.theme.palette.text.contrast : p.theme.palette.text.primary};
  font-size: 14px;
`;

const DisabledCheckbox = styled(Checkbox)`
  pointer-events: none;
`;

const Row = (props: SelectRowProps) => (
    <Container
        className={props.hovered ? "hovered" : undefined}
        $selected={props.selected}
        $hovered={props.hovered}
        onMouseDown={props.onClick}
        onMouseEnter={props.onMouseEnter}
    >
        <DisabledCheckbox
            checked={props.selected}
            readOnly={true}
            onChange={() => {}}
        />
        <Text selected={props.selected}>{props.value}</Text>
    </Container>
);

export default Row;
