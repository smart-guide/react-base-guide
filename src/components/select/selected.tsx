import React from "react";
import styled from "styled-components";
import { SelectInputProps } from "./interface";

const ElementContainer = styled.div<{ disabled: boolean }>`
  display: block;
  position: relative;
  box-sizing: border-box;
  background: ${p => p.disabled ? p.theme.palette.element.disabled : p.theme.palette.background.primary};
  cursor: text;
  pointer-events: ${p => p.disabled ? "none" : "all"};
  user-select: none;
  width: 100%;
  min-height: 36px;
  padding: 5px 10px 0;
  border: 1px solid ${p => p.theme.palette.element.border};
  border-radius: ${p => p.theme.radius.element};
  outline: none;
  transition: border 0.3s;
  
  &:hover, &:focus {
    border: 1px solid ${p => p.theme.palette.element.primary};
  }
`;

const Element = styled.div`
  display: inline-block;
  box-sizing: border-box;
  height: 28px;
  padding: 0 8px;
  border-radius: ${p => p.theme.radius.element};
  margin-right: 8px;
  margin-bottom: 5px;
  background: ${p => p.theme.palette.background.secondary};
  color: ${p => p.theme.palette.text.primary};
`;

const ElementValue = styled.span`
  display: inline-block;
  vertical-align: top;
  font-size: 14px;
  line-height: 28px;
`;

const DeleteLink = styled.a`
  display: inline-block;
  vertical-align: top;
  cursor: pointer;
  user-select: none;
  font-size: 16px;
  line-height: 28px;
  color: ${p => p.theme.palette.text.primary};
  height: 100%;
  opacity: 0.4;
  transition: opacity 0.3s;
  margin-left: 5px;

  &:hover {
    opacity: 1;
  }
`;

const rowClick = (e, onClick) => {
    e.preventDefault();
    e.stopPropagation();
    onClick();
}

const Selected = ({ rows, disabled }: SelectInputProps) => (
    <ElementContainer disabled={disabled}>
        {rows && rows.filter(row => row.selected).map((row, key) => (
            <Element key={key}>
                <ElementValue>{row.value}</ElementValue>
                <DeleteLink onClick={e => rowClick(e, row.onClick)}>&times;</DeleteLink>
            </Element>
        ))}
    </ElementContainer>
);

export default Selected;
