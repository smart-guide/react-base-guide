import styled from "styled-components";

const Wrapper = styled.div`
  position: relative;
  box-sizing: border-box;
  width: 100%;
  min-width: 200px;
  outline: none;
`;

export default Wrapper;
