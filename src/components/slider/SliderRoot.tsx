import React from "react";
import styled from "styled-components";
import { addCustomStyle, StyleProps } from "@utils/add-custom-style";

const SliderRoot = styled.div<{ $styled: StyleProps }>`
  position: relative;
  box-sizing: border-box;
  width: 100%;
  height: 300px;
  ${p => addCustomStyle(p.$styled)}
`;

export default SliderRoot;
