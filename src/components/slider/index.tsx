import React, { useState, useEffect, useRef } from "react";
import SliderRoot from "./SliderRoot";
import SlideList from "./slide-list";
import Slide from "./slide";

import { SliderProps } from "./interface";

const getIndex = (length, index) => {
    if (index < 0) {
        index = length - 1;
    }
    if (index >= length) {
        index = 0;
    }
    return index;
};

const getPerspective = (slider, setState) => {
    setState({ perspective: slider.offsetWidth * 2 + "px" });
    window.onresize = () => {
        setState({ perspective: slider.offsetWidth * 2 + "px" });
    };
}

const onSwipe = ({ length, position, state, setState }) => {
    if (Math.abs(position.x) >= 50) {
        position.x > 0 ?
            onPrev({ length, state, setState }) :
            onNext({ length, state, setState });
    }
}

const onPrev = ({ length, state, setState }) => {
    if (!state.disabledNavigation) {
        let prevIndex = state.currentIndex - 1;
        if (prevIndex < 0) {
            prevIndex = length - 1;
        }
        setState({ disabledNavigation: true, animation: "prev" });

        setTimeout(() => {
            setState({ currentIndex: prevIndex, disabledNavigation: false, animation: null });
        }, 950);
    }
}

const onNext = ({ length, state, setState }) => {
    if (!state.disabledNavigation) {
        let nextIndex = state.currentIndex + 1;
        if (nextIndex >= length) {
            nextIndex = 0;
        }
        setState({ disabledNavigation: true, animation: "next" });

        setTimeout(() => {
            setState({ currentIndex: nextIndex, disabledNavigation: false, animation: null });
        }, 950);
    }
}

const Slider = (props: SliderProps) => {
    const { children } = props;
    const [state, setAllState] = useState({
        currentIndex: 0,
        animation: null,
        disabledNavigation: false,
        perspective: null,
    });
    const setState = obj => setAllState({ ...state, ...obj });
    const slider = useRef(null);
    const { currentIndex, animation, disabledNavigation, perspective } = state;

    useEffect(() => {
        getPerspective(slider.current, setState);
    }, []);

    const onPrevFunc = () => onPrev({
        length: children.length,
        state, setState
    });

    const onNextFunc = () => onNext({
        length: children.length,
        state, setState
    });

    return (
        <SliderRoot
            id={props.id}
            className={props.className}
            $styled={props.styled}
        >
            <SlideList
                forwardRef={slider}
                animation={animation}
                perspective={perspective}
                onSwipe={position => onSwipe({
                    length: children.length,
                    position,
                    state,
                    setState
                })}
            >
                <Slide
                    position="prev"
                    onPrev={onPrevFunc}
                    onNext={onNextFunc}
                    disabledNavigation={disabledNavigation}
                >
                    {children[getIndex(children.length, currentIndex - 1)]}
                </Slide>
                <Slide
                    onPrev={onPrevFunc}
                    onNext={onNextFunc}
                    disabledNavigation={disabledNavigation}
                >
                    {children[getIndex(children.length, currentIndex)]}
                </Slide>
                <Slide
                    position="next"
                    onPrev={onPrevFunc}
                    onNext={onNextFunc}
                    disabledNavigation={disabledNavigation}
                >
                    {children[getIndex(children.length, currentIndex + 1)]}
                </Slide>
            </SlideList>
        </SliderRoot>
    )
}

export default Slider;
