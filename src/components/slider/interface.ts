import {HTMLProps, MouseEventHandler, ReactElement} from "react";
import { SwipeEvent, SwipePosition } from "react-easy-swipe";
import { StyleProps } from "@utils/add-custom-style";

interface SliderProps extends HTMLProps<HTMLDivElement> {
    children?: ReactElement[];
    styled?: StyleProps;
}

interface SlideProps {
    children?: any;
    onPrev: MouseEventHandler<HTMLButtonElement>;
    onNext: MouseEventHandler<HTMLButtonElement>;
    disabledNavigation: boolean;
    position?: "next" | "prev";
}

interface SlideListProps extends HTMLProps<HTMLDivElement> {
    forwardRef?: any;
    onSwipe?: (position: SwipePosition, e: SwipeEvent) => void;
    animation?: "next" | "prev";
    perspective: string;
}

interface SliderNavigationProps {
    onPrev: MouseEventHandler<HTMLButtonElement>;
    onNext: MouseEventHandler<HTMLButtonElement>;
    disabledNavigation: boolean;
}

export { SliderProps, SlideProps, SlideListProps, SliderNavigationProps }
