import React from "react";
import styled from "styled-components";
import ArrowIcon from "../../icons/arrow";
import { SliderNavigationProps } from "./interface";

const Wrapper = styled.div`
  position: absolute;
  height: 48px;
  width: 100%;
  top: 0;
  bottom: 0;
  margin: auto;
  z-index: 1;
  @media(max-width: ${p => p.theme.screen.mobile}) {
    height: 24px;
  }
`;

const ButtonNav = styled.button<{ left?: string, right?: string }>`
  position: absolute;
  box-sizing: border-box;
  width: 48px;
  height: 48px;
  top: 0;
  bottom: 0;
  left: ${p => p.left || "auto"};
  right: ${p => p.right || "auto"};
  padding: 0;
  border: none;
  outline: none;
  margin: auto;
  background: transparent;
  cursor: pointer;
  user-select: none;
  opacity: 0.5;
  transition: opacity 0.3s;
  @media(max-width: ${p => p.theme.screen.mobile}) {
    width: 24px;
    height: 24px;
  }

  &:hover {
    opacity: 1;
  }
  
  &:disabled {
    pointer-events: none;
    opacity: 0.5;
  }
`;

const SliderNavigation = ({ onPrev, onNext, disabledNavigation }: SliderNavigationProps) => (
    <Wrapper>
        <ButtonNav
            onClick={onPrev}
            disabled={disabledNavigation}
            left="20px"
        >
            <ArrowIcon rotate="90deg" />
        </ButtonNav>
        <ButtonNav
            onClick={onNext}
            disabled={disabledNavigation}
            right="20px"
        >
            <ArrowIcon rotate="-90deg" />
        </ButtonNav>
    </Wrapper>
);

export default SliderNavigation;
