import React from "react";
import styled, { keyframes } from "styled-components";
import Swipe from "react-easy-swipe";
import { SlideListProps } from "./interface";

const Prev = keyframes`
  100% { transform: translateX(50%) rotateY(90deg) translateX(50%); }
`;

const Next = keyframes`
  100% { transform: translateX(-50%) rotateY(-90deg) translateX(-50%); }
`;

const getAnimation = (animation) => {
    if (animation === "prev") return Prev;
    if (animation === "next") return Next;
    return "none";
}

const SwipeWrapper = styled<any>(Swipe)`
  position: relative;
  box-sizing: border-box;
  width: 100%;
  height: 100%;
`;

const Wrapper = styled.div<{ perspective?: string }>`
  position: absolute;
  box-sizing: border-box;
  width: 100%;
  height: 100%;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  margin: auto;
  perspective: ${p => p.perspective || "2000px"};
  perspective-origin: center center;
`;

const List = styled.div<{ animation?: string }>`
  position: relative;
  box-sizing: border-box;
  width: 100%;
  height: 100%;
  transform-style: preserve-3d;
  animation-name: ${p => getAnimation(p.animation)};
  animation-duration: 1s;
`;

const SlideList = ({ children, forwardRef, className, onSwipe, animation, perspective }: SlideListProps) => {
    return (
        <SwipeWrapper
            className={className}
            onSwipeMove={onSwipe}
        >
            <Wrapper ref={forwardRef} perspective={perspective}>
                <List animation={animation}>
                    {children}
                </List>
            </Wrapper>
        </SwipeWrapper>
    );
}

export default SlideList;
