import React from "react";
import styled from "styled-components";
import Navigation from "./navigation";
import { SlideProps } from "./interface";


const getTransform = (position) => {
    if (position === "prev") return "transform: translateX(-50%) rotateY(270deg) translateX(-50%)";
    if (position === "next") return "transform: translateX(50%) rotateY(90deg) translateX(50%)";
    return "transform: rotateY(0deg)";
}

const Wrapper = styled.div<{position?: "prev" | "next"}>`
  position: absolute;
  box-sizing: border-box;
  width: 100%;
  height: 100%;
  ${p => getTransform(p.position)};
  
  >* {
    width: 100%;
    height: 100%;
  }
`;

const Slide = ({ children, onPrev, onNext, disabledNavigation, position }: SlideProps) => (
    <Wrapper position={position}>
        {children}
        <Navigation
            onPrev={onPrev}
            onNext={onNext}
            disabledNavigation={disabledNavigation}
        />
    </Wrapper>
)

export default Slide;
