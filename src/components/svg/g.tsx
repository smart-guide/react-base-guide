import React from "react";
import styled from "styled-components";
import { GProps } from "./interface";

const G = styled.g<GProps>`
  fill: ${p => p.theme.palette.text[p.fill] || p.theme.palette.text.primary};    
  stroke: ${p => p.theme.palette.text[p.stroke] || "none"};
  transition: fill 0.3s, stroke 0.3s;
`;

export default G;
