import Svg from "./svg";
import Path from "./path";
import Rect from "./rect";
import Polygon from "./polygon";
import Polyline from "./polyline";
import Circle from "./circle";
import G from "./g";

import { IconProps, TextColor } from "./interface";

export {
    Svg,
    Path,
    Rect,
    Polygon,
    Polyline,
    Circle,
    G
}

export type {
    IconProps,
    TextColor
}
