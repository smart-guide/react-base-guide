import {MouseEventHandler} from "react";
import { StyleProps } from "@utils/add-custom-style";

export type TextColor = "primary" | "secondary" | "active" | "error" | "warning" | "success" | "contrast" | "placeholder" | "disabled";

export interface IconProps {
    disabled?: boolean;
    className?: string;
    verticalAlign?: string;
    cursor?: string;
    pointerEvents?: string;
    rotate?: string;
    fill?: TextColor;
    stroke?: TextColor;
    styled?: StyleProps;
    onClick?: MouseEventHandler;
}

export interface SvgProps {
    disabled?: boolean;
    viewBox: string;
    height: string;
    width: string;
    children?: any;
    verticalAlign?: string;
    cursor?: string;
    pointerEvents?: string;
    rotate?: string;
    transform?: string;
    fill?: TextColor;
    stroke?: TextColor;
    enableBackground?: string;
    styled?: StyleProps;
    onClick?: MouseEventHandler;
}

export interface PathProps {
    id?: string;
    className?: string;
    fill?: TextColor;
    stroke?: TextColor;
    strokeWidth?: string;
    transform?: string;
    fillRule?: string;
    clipRule?: string;
    strokeLinecap?: string;
    strokeLinejoin?: string;
    d?: string;
}

export interface RectProps {
    id?: string;
    className?: string;
    fill?: TextColor;
    stroke?: TextColor;
    strokeWidth?: string;
    transform?: string;
    mask?: string;

    width?: string;
    height?: string;
    x?: string;
    y?: string;
    rx?: string;
    ry?: string;
}

export interface PolygonProps {
    id?: string;
    className?: string;
    fill?: TextColor;
    stroke?: TextColor;

    points?: string;
    pathLength?: number;
    transform?: string;
}

export interface PolylineProps {
    id?: string;
    className?: string;
    fill?: TextColor;
    stroke?: TextColor;
    strokeWidth?: number;
    strokeMiterlimit?: number;
    points?: string;
    pathLength?: number;
    transform?: string;
}

export interface CircleProps {
    id?: string;
    className?: string;
    fill?: TextColor;
    stroke?: TextColor;
    strokeWidth?: string;
    transform?: string;

    cx?: string;
    cy?: string;
    r?: string;
}

export interface GProps {
    id?: string;
    children?: any;
    className?: string;
    fill?: TextColor;
    stroke?: TextColor;
    transform?: string;
}
