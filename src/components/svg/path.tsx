import React from "react";
import styled from "styled-components";
import { PathProps } from "./interface";

const Path = styled.path<PathProps>`
  fill: ${p => p.theme.palette.text[p.fill] || "none"};    
  stroke: ${p => p.theme.palette.text[p.stroke] || "none"};
  transition: fill 0.3s, stroke 0.3s;
`;

export default Path;
