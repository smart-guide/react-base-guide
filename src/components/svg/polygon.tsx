import React from "react";
import styled from "styled-components";
import { PolygonProps } from "./interface";

const Polygon = styled.polygon<PolygonProps>`
  fill: ${p => p.theme.palette.text[p.fill] || "none"};
  stroke: ${p => p.theme.palette.text[p.stroke] || "none"};
  transition: fill 0.3s, stroke 0.3s;
`;

export default Polygon;
