import React from "react";
import styled, { keyframes } from "styled-components";
import { PolylineProps } from "./interface";

const StrokeAnimation = keyframes`
  0% { stroke-dasharray: 0 300; }
  100% { stroke-dasharray: 300 0; }
`;

const Polyline = styled.polyline<PolylineProps>`
  fill: ${p => p.theme.palette.text[p.fill] || "none"};
  stroke: ${p => p.theme.palette.text[p.stroke] || "none"};
  transition: fill 0.3s, stroke 0.3s;
  animation: ${StrokeAnimation} 3s alternate;
`;

export default Polyline;
