import React from "react";
import styled from "styled-components";
import { RectProps } from "./interface";

const Rect = styled.rect<RectProps>`
  fill: ${p => p.theme.palette.text[p.fill] || "none"};
  stroke: ${p => p.theme.palette.text[p.stroke] || "none"};
  stroke-width: ${p => p.strokeWidth};
  width: ${p => p.width};
  height: ${p => p.height};
  transition: fill 0.3s, stroke 0.3s;
`;

export default Rect;
