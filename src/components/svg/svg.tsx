import React from "react";
import styled from "styled-components";
import addCustomStyle from "@utils/add-custom-style";
import { SvgProps } from "./interface";


const Svg = styled.svg<SvgProps>`
  cursor: ${p => p.cursor || "default"};
  pointer-events: ${p => p.disabled ? "none" : (p.pointerEvents || "all")};
  vertical-align: ${p => p.verticalAlign || "baseline"};
  box-sizing: border-box;
  
  fill: ${p => p.fill};
  stroke-width: ${p => p.strokeWidth};
  stroke-miterlimit: ${p => p.strokeMiterlimit};
  enable-background: ${p => p.enableBackground};
  
  transform: rotate(${p => p.rotate || 0});
  transition: transform 0.3s;  
  
  ${p => addCustomStyle(p.styled)}
`;

export default Svg;
