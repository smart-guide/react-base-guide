import React from "react";
import styled from "styled-components";
import TableProps from "./interface";
import { addCustomStyle, StyleProps } from "@utils/add-custom-style";

const Wrapper = styled.table<{ $styled: StyleProps }>`
  position: relative;
  box-sizing: border-box;
  border-collapse: collapse;
  border-spacing: 0;

  ${p => addCustomStyle(p.$styled)}
`;

const TableHead = styled.thead`
  box-sizing: border-box;
  font-size: 12px;
  text-transform: uppercase;
  color: ${p => p.theme.palette.text.secondary};
`;

const Body = styled.tbody`
  box-sizing: border-box;
  font-size: 14px;
  color: ${p => p.theme.palette.text.primary};
`;

const Row = styled.tr`
  box-sizing: border-box;
  border-top: 1px solid ${p => p.theme.palette.element.border};
  &:first-of-type {
    border-bottom: none;
  }
`;

const Col = styled.td`
  position: relative;
  box-sizing: border-box;
  padding: 16px 12px;
  vertical-align: top;
  
  @media(max-width: ${p => p.theme.screen.mobile}) {
    padding: 8px 8px;
  }
`;

const Table = (props: TableProps) => {
    const { head, body } = props;

    return (
        <Wrapper
            id={props.id}
            className={props.className}
            $styled={props.styled}
        >
            <TableHead>
                <Row>
                    {head && Object.keys(head).map((col, key) => <Col key={key}>{head[col]}</Col>)}
                </Row>
            </TableHead>
            <Body>
                {body && body.map((row, rowKey) => (
                    <Row key={rowKey}>
                        {head && Object.keys(head).map((col, key) => <Col key={key}>{row[col]}</Col>)}
                    </Row>
                ))}
            </Body>
        </Wrapper>
    );
}

export default Table;
