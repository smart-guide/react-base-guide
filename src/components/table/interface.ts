import { StyleProps } from "@utils/add-custom-style";

interface TableProps {
    id?: string,
    name?: string,
    className?: string,

    head?: object,
    body?: any[],

    styled?: StyleProps
}

export default TableProps;
