import styled from "styled-components";
import addCustomStyle from "@utils/add-custom-style";
import TextareaProps from "./interface";

const getBase = (p) => `
  display: block;
  position: relative;
  box-sizing: border-box;
  width: 100%;
  height: ${p.autosize ? (p.height || "38px") : "38px"};
  min-height: ${p.minHeight || "38px"};
  padding: 8px 16px;
  padding-bottom: 6px;
  border-radius: ${p.theme.radius.element};
  outline: none;
  font-size: 14px;
  line-height: 22px;
  transition: height 0.1s, border 0.3s;
  white-space: pre-wrap;
  word-wrap: break-word;
`;

const TextareaBase = styled.textarea<TextareaProps>`
  ${p => getBase(p)}
  background: transparent;
  color: ${p => p.theme.palette.text.primary};
  overflow: ${p => p.autosize ? "hidden" : "auto"};
  resize: none;
  border: 1px solid transparent;

  &::placeholder {
    color: ${p => p.theme.palette.text.placeholder};
  }
    
  &:disabled {
    background: ${p => p.theme.palette.element.disabled};
    border-color: ${p => p.theme.palette.element.disabled};
    color: ${p => p.theme.palette.text.secondary};
    pointer-events: none;
  }
  
  ${p => addCustomStyle(p.styled)}
`;

const TextareaHidden = styled.div`
  ${p => getBase(p)}
  border: 1px solid ${p => p.theme.palette.element.primary};
  position: absolute;
  pointer-events: none;
  height: auto;
  opacity: 0;
  top: 0;
  left: 0;
`;

export { TextareaBase, TextareaHidden };
