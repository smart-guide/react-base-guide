import React, { forwardRef, useState, useRef, useEffect } from "react";
import { TextareaBase, TextareaHidden } from "./base";
import Message from "@components/input/styled/Message";
import TextareaProps from "./interface";
import Label from "@components/label";

const textareaProps = (props: TextareaProps): TextareaProps => {
    let newProps = {...props};
    delete newProps.label;
    delete newProps.message;
    delete newProps.styled;
    return newProps;
}

const Textarea = (props: TextareaProps, ref: any) => {
    const textareaHidden = useRef(null);
    const [height, setHeight] = useState<number>(props.styled?.height ? +props.styled?.height?.replace("px", "") : undefined);
    const [focus, setFocus] = useState<boolean>(props.autoFocus || false);

    useEffect(() => {
        if (props.autosize && textareaHidden.current) {
            textareaHidden.current.innerText = props.value;
            setHeight(textareaHidden.current.clientHeight);
        }
    }, [props.autosize]);

    return (
        <Label
            label={props.label}
            focus={focus}
            tabIndex={props.tabIndex || 0}
            variant="outlined"
            error={props.error}
            warning={props.warning}
            success={props.success}
            styled={{
                ...props.styled,
                height: height ? (height + 7.5) + "px" : undefined,
            }}
        >
            {props.message &&
                <Message
                    $error={props.error}
                    $success={props.success}
                    $warning={props.warning}
                >{props.message}</Message>
            }

            {props.autosize && <TextareaHidden ref={textareaHidden}>{props.value || "A"}</TextareaHidden>}

            <TextareaBase
                {...textareaProps(props)}

                onChange={e => {
                    if (props.autosize && textareaHidden.current) {
                        textareaHidden.current.innerText = e.target.value;
                        setHeight(textareaHidden.current.clientHeight);
                    }
                    props.onChange && props.onChange(e);
                }}

                ref={ref}
                placeholder={props.placeholder}
                onFocus={e => {
                    setFocus(true);
                    props.onFocus && props.onFocus(e);
                }}
                onBlur={e => {
                    setFocus(false);
                    props.onBlur && props.onBlur(e);
                }}
                styled={{
                    height: height ? (height + "px") : undefined,
                    minHeight: props?.styled?.minHeight,
                }}
            />
        </Label>
    )
};

export default forwardRef(Textarea);
