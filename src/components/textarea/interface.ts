import {TextareaHTMLAttributes} from "react";
import {StyleProps} from "@utils/add-custom-style";

interface TextareaProps extends TextareaHTMLAttributes<HTMLTextAreaElement> {
    label?: string
    message?: string

    error?: boolean
    success?: boolean
    warning?: boolean

    autosize?: boolean

    autoFocus?: boolean
    autoComplete?: "on" | "off"

    styled?: StyleProps
}

export default TextareaProps;
