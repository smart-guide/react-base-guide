import styled, { keyframes } from "styled-components";

const show = keyframes`
  16% {transform: translate(-50%,8px)}
  33% {transform: translate(-50%,-6px)}
  50% {transform: translate(-50%,4px)}
  66% {transform: translate(-50%,-2px)}
  83% {transform: translate(-50%,1px)}
  100% {transform: translate(-50%,0)}
`;

const hide = keyframes`
  0% {transform: translate(-50%,20px)}
  100% {transform: translate(-50%,-100px)}
`;

const ToastStyled = styled.div<{ $color: string, $hide: boolean }>`
  display: block;
  position: fixed;
  box-sizing: border-box;
  top: 24px;
  left: 50%;
  transform: translate(-50%, 0);
  background: ${p => p.theme.palette.element[p.$color] || p.theme.palette.element.primary};
  color: ${p => p.theme.palette.text.contrast};
  padding: 10px 36px;
  border-radius: ${p => p.theme.radius.element};
  margin: auto;
  box-shadow: ${p => p.theme.shadow.absolute};
  font-size: 14px;
  line-height: 1.5;
  text-align: center;
  z-index: ${p => p.theme.zIndex.push};
  animation: ${p => p.$hide ? hide : show} 1s ease-in-out;
  
  @media (max-width: ${p => p.theme.screen.mobile}) {
    padding: 10px;
    max-width: 90%;
    width: 300px;
  }
`;

export default ToastStyled;
