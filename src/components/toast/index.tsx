import React, { useState, useEffect } from "react";
import BaseToast from "./base";
import IToast from "./interface";

const timeout = 3700;

const Toast = (props: IToast) => {
    const [ hide, setHide ] = useState(false);

    useEffect(() => {
        setTimeout(() => setHide(true), timeout - 700);
        setTimeout(() => { props.onClose && props.onClose(); }, timeout)
    }, []);

    return (
        <BaseToast
            $color={props.color}
            $hide={hide}
        >
            {props.children}
        </BaseToast>
    );
}

export default Toast;
