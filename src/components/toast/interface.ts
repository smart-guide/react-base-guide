import {HTMLProps} from "react";

export interface ToastProps extends HTMLProps<HTMLDivElement> {
    color?: "primary" | "error" | "warning" | "success";
    onClose?: Function
}

export default ToastProps;
