import React from "react";
import { Svg, Polyline, IconProps } from "@components/svg";

const ArrowIcon = (props: IconProps) =>
    <Svg
        viewBox="0 0 50 50"
        width={props.styled?.width || "12px"}
        height={props.styled?.height || "12px"}
        rotate={props.rotate}
        {...props}
    >
        <Polyline
            className={props.className ? props.className + " rgb-stroke" : "rgb-stroke"}
            points="5,12 25,32 45,12"
            stroke={props.stroke || "primary"}
            strokeWidth={4}
            strokeMiterlimit={10}
        />
    </Svg>;


export default ArrowIcon;
