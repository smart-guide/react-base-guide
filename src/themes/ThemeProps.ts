import { DefaultTheme } from "styled-components"

interface Screen {
    desktop: string;
    tablet: string;
    mobile: string;
}

interface Radius {
    element: string;
    area: string;
    round: string
}

interface FontSize {
    secondary: string;
    primary: string;
    title: string;
    primaryTitle: string;
}

interface ZIndex {
    push: number;
    modal: number;
    fixed: number;
    dropdown: number;
    absolute: number;
    loader: number;
    active: number;
    relative: number;
}

interface Palette {
    background: {
        primary: string;
        secondary: string;
        active: string;
        error: string;
        warning: string;
        success: string;
        contrast: string;
    }

    text: {
        primary: string;
        secondary: string;
        active: string;
        error: string;
        warning: string;
        success: string;
        contrast: string;
        placeholder: string;
        disabled: string;
    }

    element: {
        background: string;
        primary: string;
        primaryHover: string;
        divider: string;
        border: string;
        disabled: string;
        error: string;
        warning: string;
        success: string;
    }
}

interface Shadow {
    element: string;
    absolute: string;
    container: string;
}

interface Theme extends DefaultTheme {
    screen: Screen;
    radius: Radius;
    fontSize: FontSize;
    zIndex: ZIndex;
    palette: Palette;
    shadow: Shadow;
}

declare module "styled-components" {
    export interface DefaultTheme {
        screen: Screen;
        radius: Radius;
        fontSize: FontSize;
        zIndex: ZIndex;
        palette: Palette;
        shadow: Shadow;
    }
}

export default Theme;
