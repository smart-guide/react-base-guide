const theme = {
    screen: {
        desktop: "1440px",
        tablet: "1024px",
        mobile: "767px"
    },

    radius: {
        element: "4px",
        area: "16px",
        round: "50%"
    },

    fontSize: {
        secondary: "12px",
        primary: "14px",
        title: "18px",
        primaryTitle: "24px",
    },

    zIndex: {
        push: 1090,
        modal: 1080,
        fixed: 1070,
        dropdown: 1060,
        absolute: 1050,
        loader: 1040,
        active: 1010,
        relative: 1001,
    }
}

export default theme;
