import base from "./base";
import { DefaultTheme } from "styled-components";

const dark: DefaultTheme = {
    ...base,

    palette: {
        background: {
            primary: "#172131",
            secondary: "#1F2C41",
            active: "#101722",
            error: "#320606",
            warning: "#412002",
            success: "#002e08",
            contrast: "#0B1018",
        },

        text: {
            primary: "#F5F4F7",
            secondary: "#ABAAAC",
            active: "#3B7AF4",
            error: "#FF5353",
            warning: "#FBAB5E",
            success: "#00A820",
            contrast: "#F5F4F7",
            placeholder: "#ABAAAC",
            disabled: "#ABAAAC",
        },

        element: {
            background: "#101722",
            primary: "#3B7AF4",
            primaryHover: "#1560E2",
            divider: "#5C636E",
            border: "#5C636E",
            disabled: "#3D5874",
            error: "#FF5353",
            warning: "#FBAB5E",
            success: "#00A820",
        },
    },

    shadow: {
        element: "0px 2px 4px rgba(17, 17, 17, 0.08)",
        absolute: "0px 6px 16px rgba(0, 0, 0, 0.08), 0px 3px 6px -4px rgba(0, 0, 0, 0.08);",
        container: "0px 6px 48px rgba(0, 0, 0, 0.07)"
    },
}

export default dark;
