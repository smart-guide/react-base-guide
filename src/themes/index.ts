import light from "./light";
import dark from "./dark";
import ThemeProps from "./ThemeProps";

const theme = {
    light,
    dark,
}

export type { ThemeProps };

export default theme;
