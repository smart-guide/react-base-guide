import base from "./base";
import { DefaultTheme } from "styled-components";

const light: DefaultTheme = {
    ...base,

    palette: {
        background: {
            primary: "#FFFFFF",
            secondary: "#F5F4F7",
            active: "#EEF3FD",
            error: "#FFF3F3",
            warning: "#fCC897",
            success: "#E1FFDE",
            contrast: "#3F4354",
        },

        text: {
            primary: "#3F4354",
            secondary: "#6A6D82",
            active: "#3B7AF4",
            error: "#FF5353",
            warning: "#FBAB5E",
            success: "#00A820",
            contrast: "#FFFFFF",
            placeholder: "#CDCBD6",
            disabled: "#6A6D82",
        },

        element: {
            background: "#FFFFFF",
            primary: "#3B7AF4",
            primaryHover: "#1560E2",
            divider: "#EAE8EE",
            border: "#DBDAE2",
            disabled: "#CDCBD6",
            error: "#FF5353",
            warning: "#FBAB5E",
            success: "#00A820",
        },
    },

    shadow: {
        element: "0px 2px 4px 2px rgba(17, 17, 17, 0.08)",
        absolute: "0px 6px 16px rgba(0, 0, 0, 0.08), 0px 3px 6px -4px rgba(0, 0, 0, 0.08);",
        container: "0px 6px 48px rgba(0, 0, 0, 0.07)"
    },
}

export default light;
