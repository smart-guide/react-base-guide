export interface StyleProps {
  display?: "inline" | "inline-block" | "block" | "inline-flex" | "flex" | "inline-grid" | "grid" | "none";

  width?: string;
  minWidth?: string;
  maxWidth?: string;

  height?: string;
  minHeight?: string;
  maxHeight?: string;

  margin?: string;
  marginTop?: string;
  marginBottom?: string;
  marginLeft?: string;
  marginRight?: string;
}

const addCssProp = (propsName: string, value: string): string => {
    if (!propsName || !value) { return ""; }
    return `${propsName}: ${value};`;
}

export const addCustomStyle = (p: StyleProps, displayDefault?: string): string => `
  ${addCssProp("display", p?.display || displayDefault)}
  ${addCssProp("width", p?.width)}
  ${addCssProp("min-width", p?.minWidth)}
  ${addCssProp("max-width", p?.maxWidth)}
  ${addCssProp("height", p?.height)}
  ${addCssProp("min-height", p?.minHeight)}
  ${addCssProp("max-height", p?.maxHeight)}
  ${addCssProp("margin", p?.margin)}
  ${addCssProp("margin-top", p?.marginTop)}
  ${addCssProp("margin-bottom", p?.marginBottom)}
  ${addCssProp("margin-left", p?.marginLeft)}
  ${addCssProp("margin-right", p?.marginRight)}
`;

export default addCustomStyle;
