import {Dispatch, KeyboardEvent, SetStateAction} from "react";
import {DropdownRowProps} from "@components/dropdown-menu/interface";

interface DropdownMenuKeysHandlerProps {
    e: KeyboardEvent;
    setOpen:  Dispatch<SetStateAction<boolean>>;
    rows: DropdownRowProps[]
}

const dropdownMenuKeysHandler = ({ e, setOpen, rows }: DropdownMenuKeysHandlerProps) => {
    if (!rows) { return; }

    if (e.key === "Enter" || e.key === "Escape") {
        e.preventDefault();
        setOpen(false);
        return;
    }

    if (!(e.key === "ArrowUp" || e.key === "ArrowDown")) {
        return;
    }

    if (rows?.filter(r => r.selected).length === 0 && e.key === "ArrowUp" || e.key === "ArrowDown") {
        rows[0]?.onClick && rows[0]?.onClick(e as any);
    }

    for (let index = 0; index < rows.length; index++) {
        let row = rows[index];
        if (row.selected) {
            if (e.key === "ArrowUp") {
                e.preventDefault();
                let prevRow = rows[index - 1] || row;
                prevRow.onClick && prevRow.onClick(e as any);
            }
            if (e.key === "ArrowDown") {
                e.preventDefault();
                let nextRow = rows[index + 1] || row;
                nextRow.onClick && nextRow.onClick(e as any);
            }
            break;
        }
    }
}

export default dropdownMenuKeysHandler;
