const getBorderColor = (p) => {
    if (p.disabled || p.$disabled) { return p.theme.palette.element.border; }
    if (p.error || p.$error) { return p.theme.palette.element.error; }
    if (p.warning || p.$warning) { return p.theme.palette.element.warning; }
    if (p.success || p.$success) { return p.theme.palette.element.success; }
    if (p.hover || p.focus || p.$hover || p.$focus) { return p.theme.palette.element.primary; }
    return p.theme.palette.element.border;
}

export default getBorderColor;
