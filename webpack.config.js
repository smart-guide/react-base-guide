const path = require("path");
const webpack = require("webpack");
const TerserPlugin = require("terser-webpack-plugin");
const NodemonPlugin = require("nodemon-webpack-plugin");

const locations = [
    path.resolve(__dirname, "frontend"),
    path.resolve(__dirname, "backend"),
    path.resolve(__dirname, "src"),
];

const getPlugins = (isServer) => {
    let plugins = [
        new webpack.DefinePlugin({ dirName: "__dirname" })
    ];

    if (isServer) {
        plugins.push(new NodemonPlugin({
            script: "./build/server.js",
            ignore: ["*.js.map"],
        }));
    }

    return plugins;
}

const common = ({ mode = "development" }) => ({
    mode: mode,

    watchOptions: {
        ignored: /node_modules/
    },

    stats: {
        children: false,
        warningsFilter: /^(?!CriticalDependenciesWarning$)/
    },

    optimization: {
        minimize: mode === "production",
        minimizer: [new TerserPlugin({ extractComments: false })],
        mangleWasmImports: mode === "production",
        removeAvailableModules: mode === "production",
        nodeEnv: mode === "production" ? "production" : "development",

        splitChunks: {
            cacheGroups: {
                defaultVendors: false
            }
        }
    },
    resolve: {
        modules: ["node_modules"],
        extensions: [".js", ".jsx", ".ts", ".tsx"],

        alias: {
            "@interfaces": path.resolve(__dirname, "src/interfaces/index.ts"),
            "@components": path.resolve(__dirname, "src/components"),
            "@utils": path.resolve(__dirname, "src/utils"),
        }
    },
    resolveLoader: {
        modules: ["node_modules"],
        extensions: [".js", ".jsx", ".ts", ".tsx"],
    },
    module: {
        rules: [
            {
                test: [/\.jsx?$/, /\.js$/, /\.tsx?$/, /\.ts?$/],
                include: locations,
                loader: "babel-loader",
                options: { configFile: path.resolve(__dirname, "babel.config.js") },
            },
            {
                test: /\.(png|jpg|jpeg|gif|otf|mp4|webp|woff|woff2|ttf|ico)$/,
                include: locations,
                type: "asset/resource",
            },
            {
                test: /\.(svg)$/,
                include: locations,
                type: "asset/inline",
            }
        ],
    }
});

const frontend = {
    entry: "./frontend",
    output: {
        path: path.resolve(__dirname, "build/public"),
        publicPath: "/",
        filename: "bundle.js",
        chunkFilename: "chunk-[name].js",
        assetModuleFilename: "images/[name][ext][query]"
    },
    plugins: getPlugins(false)
};

const backend = {
    entry: "./backend",
    output: {
        path: path.resolve(__dirname, "build"),
        publicPath: "/",
        filename: "server.js",
        chunkFilename: "[name]-chunk.server.js",
        assetModuleFilename: "images/[name][ext][query]"
    },
    target: "node",
    plugins: getPlugins(true)
};

const lib = {
    entry: "./src/components/index.tsx",
    devtool: "inline-source-map",
    output: {
        path: path.resolve(__dirname, "lib"),
        publicPath: "/",
        filename: "index.js",
        umdNamedDefine: true,
        library: {
            type: "commonjs2",
        },
    },
    plugins: getPlugins(false),
    externals: [
        "react",
        "react-router-dom",
        "react-is",
        "styled-components"
    ]
};

module.exports = () => {
    const mode = process.env.NODE_ENV;
    const execCommon = common({ mode });

    return [
        Object.assign({}, execCommon, frontend),
        Object.assign({}, execCommon, backend),
        Object.assign({}, common({ mode }), lib),
    ];
};
